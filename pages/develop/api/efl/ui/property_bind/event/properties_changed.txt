~~Title: Efl.Ui.Property_Bind: properties,changed~~

===== Description =====

%%Event dispatched when a property on the object has changed due to a user interaction on the object that a model could be interested in.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:property_bind:event:properties_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
properties,changed: Efl.Ui.Property_Event;
</code>

===== C information =====

<code c>
EFL_UI_PROPERTY_BIND_EVENT_PROPERTIES_CHANGED(Efl_Ui_Property_Event)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_property_bind_event_properties_changed(void *data, const Efl_Event *event)
{
    Efl_Ui_Property_Event info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_PROPERTY_BIND_EVENT_PROPERTIES_CHANGED, on_efl_ui_property_bind_event_properties_changed, d);
}

</code>
