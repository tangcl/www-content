~~Title: Efl.Ui.Bg.key~~
====== Efl.Ui.Bg.key ======

===== Description =====

%%The key which corresponds to the target data within a file.%%

%%Some file types can contain multiple data streams which are indexed by a key. Use this property for such cases (See for example %%[[:develop:api:efl:ui:image|Efl.Ui.Image]]%% or %%[[:develop:api:efl:ui:layout|Efl.Ui.Layout]]%%).%%

%%You must not modify the strings on the returned pointers.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:bg:property:key:description&nouser&nolink&nodate}}

===== Values =====

  * **key** - %%The group that the data belongs to. See the class documentation for particular implementations of this interface to see how this property is used.%%

//Overridden from [[:develop:api:efl:file:property:key|Efl.File.key]] **(get, set)**.//===== Signature =====

<code>
@property key {
    get {}
    set {}
    values {
        key: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_file_key_get(const Eo *obj);
void efl_file_key_set(Eo *obj, const char *key);
</code>

===== Implemented by =====

  * [[:develop:api:efl:file:property:key|Efl.File.key]]
  * [[:develop:api:efl:ui:win_part:property:key|Efl.Ui.Win_Part.key]]
  * [[:develop:api:efl:ui:layout:property:key|Efl.Ui.Layout.key]]
  * [[:develop:api:efl:ui:widget_part_bg:property:key|Efl.Ui.Widget_Part_Bg.key]]
  * [[:develop:api:efl:ui:popup_part_backwall:property:key|Efl.Ui.Popup_Part_Backwall.key]]
  * [[:develop:api:efl:ui:bg:property:key|Efl.Ui.Bg.key]]

