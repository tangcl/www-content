~~Title: Efl.Ui.Widget.on_access_update~~
====== Efl.Ui.Widget.on_access_update ======

===== Description =====

%%Hook function called when accessibility is changed on the widget.%%

%%This meant to be overridden by subclasses to support accessibility. This is an unstable API.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:on_access_update:description&nouser&nolink&nodate}}

===== Signature =====

<code>
on_access_update @beta @protected {
    params {
        @in enable: bool;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_widget_on_access_update(Eo *obj, Eina_Bool enable);
</code>

===== Parameters =====

  * **enable** //(in)// - %%%%''true''%% if accessibility is enabled.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:on_access_update|Efl.Ui.Widget.on_access_update]]
  * [[:develop:api:efl:ui:panel:method:on_access_update|Efl.Ui.Panel.on_access_update]]
  * [[:develop:api:efl:ui:calendar:method:on_access_update|Efl.Ui.Calendar.on_access_update]]

