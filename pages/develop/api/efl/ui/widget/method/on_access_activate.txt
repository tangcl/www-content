~~Title: Efl.Ui.Widget.on_access_activate~~
====== Efl.Ui.Widget.on_access_activate ======

===== Description =====

%%Hook function called when widget is activated through accessibility.%%

%%This meant to be overridden by subclasses to support accessibility. This is an unstable API.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:method:on_access_activate:description&nouser&nolink&nodate}}

===== Signature =====

<code>
on_access_activate @beta @protected {
    params {
        @in act: Efl.Ui.Activate;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_widget_on_access_activate(Eo *obj, Efl_Ui_Activate act);
</code>

===== Parameters =====

  * **act** //(in)// - %%Type of activation.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:on_access_activate|Efl.Ui.Widget.on_access_activate]]
  * [[:develop:api:efl:ui:slider:method:on_access_activate|Efl.Ui.Slider.on_access_activate]]
  * [[:develop:api:efl:ui:check:method:on_access_activate|Efl.Ui.Check.on_access_activate]]
  * [[:develop:api:efl:ui:radio:method:on_access_activate|Efl.Ui.Radio.on_access_activate]]
  * [[:develop:api:efl:ui:text:method:on_access_activate|Efl.Ui.Text.on_access_activate]]
  * [[:develop:api:efl:ui:slider_interval:method:on_access_activate|Efl.Ui.Slider_Interval.on_access_activate]]
  * [[:develop:api:efl:ui:button:method:on_access_activate|Efl.Ui.Button.on_access_activate]]

