~~Title: Efl.Ui.Datepicker: date,changed~~

===== Description =====

%%Called when date value is changed%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:datepicker:event:date_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
date,changed;
</code>

===== C information =====

<code c>
EFL_UI_DATEPICKER_EVENT_DATE_CHANGED(void)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_datepicker_event_date_changed(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_DATEPICKER_EVENT_DATE_CHANGED, on_efl_ui_datepicker_event_date_changed, d);
}

</code>
