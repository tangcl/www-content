~~Title: Efl.Ui.Datepicker.date_max~~
====== Efl.Ui.Datepicker.date_max ======

===== Description =====

%%The upper boundary of date.%%

%%%%''year''%%: Year. The year range is from 1900 to 2137.%%

%%%%''month''%%: Month. The month range is from 1 to 12.%%

%%%%''day''%%: Day. The day range is from 1 to 31 according to %%''month''%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:datepicker:property:date_max:description&nouser&nolink&nodate}}

===== Values =====

  * **year** - %%The year value.%%
  * **month** - %%The month value from 1 to 12.%%
  * **day** - %%The day value from 1 to 31.%%

===== Signature =====

<code>
@property date_max {
    get {}
    set {}
    values {
        year: int;
        month: int;
        day: int;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_datepicker_date_max_get(const Eo *obj, int *year, int *month, int *day);
void efl_ui_datepicker_date_max_set(Eo *obj, int year, int month, int day);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:datepicker:property:date_max|Efl.Ui.Datepicker.date_max]]

