~~Title: Efl.Ui.Collection.fallback_selection~~
====== Efl.Ui.Collection.fallback_selection ======

===== Description =====

%%A object that will be selected in case nothing is selected%%

%%A object set to this property will be selected instead of no item being selected. Which means, there will be always at least one element selected. If this property is %%''NULL''%%, the state of "no item is selected" can be reached.%%

%%Setting this property as a result of selection events results in undefined behavior.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:collection:property:fallback_selection:description&nouser&nolink&nodate}}

===== Values =====

  * **fallback** - No description supplied.

//Overridden from [[:develop:api:efl:ui:single_selectable:property:fallback_selection|Efl.Ui.Single_Selectable.fallback_selection]] **(get, set)**.//===== Signature =====

<code>
@property fallback_selection @pure_virtual {
    get {}
    set {}
    values {
        fallback: Efl.Ui.Selectable;
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Selectable *efl_ui_selectable_fallback_selection_get(const Eo *obj);
void efl_ui_selectable_fallback_selection_set(Eo *obj, Efl_Ui_Selectable *fallback);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:single_selectable:property:fallback_selection|Efl.Ui.Single_Selectable.fallback_selection]]
  * [[:develop:api:efl:ui:collection:property:fallback_selection|Efl.Ui.Collection.fallback_selection]]
  * [[:develop:api:efl:ui:select_model:property:fallback_selection|Efl.Ui.Select_Model.fallback_selection]]
  * [[:develop:api:efl:ui:tab_bar:property:fallback_selection|Efl.Ui.Tab_Bar.fallback_selection]]
  * [[:develop:api:efl:ui:radio_group_impl:property:fallback_selection|Efl.Ui.Radio_Group_Impl.fallback_selection]]

