~~Title: Efl.Ui.Collection.pack_before~~
====== Efl.Ui.Collection.pack_before ======

===== Description =====

%%Prepend an object before the %%''existing''%% sub-object.%%

%%When this container is deleted, it will request deletion of the given %%''subobj''%%. Use %%[[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]%% to remove %%''subobj''%% from this container without deleting it.%%

%%If %%''existing''%% is %%''NULL''%% this method behaves like %%[[:develop:api:efl:pack_linear:method:pack_begin|Efl.Pack_Linear.pack_begin]]%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:collection:method:pack_before:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_linear:method:pack_before|Efl.Pack_Linear.pack_before]].//===== Signature =====

<code>
pack_before @pure_virtual {
    params {
        @in subobj: Efl.Gfx.Entity;
        @in existing: const(Efl.Gfx.Entity);
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_before(Eo *obj, Efl_Gfx_Entity *subobj, const Efl_Gfx_Entity *existing);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%Object to pack before %%''existing''%%.%%
  * **existing** //(in)// - %%Existing reference sub-object. Must already belong to the container or be %%''NULL''%%.%%

===== Implemented by =====

  * [[:develop:api:efl:pack_linear:method:pack_before|Efl.Pack_Linear.pack_before]]
  * [[:develop:api:efl:ui:tab_bar:method:pack_before|Efl.Ui.Tab_Bar.pack_before]]
  * [[:develop:api:efl:ui:flip:method:pack_before|Efl.Ui.Flip.pack_before]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_before|Efl.Canvas.Layout_Part_Box.pack_before]]
  * [[:develop:api:efl:ui:box:method:pack_before|Efl.Ui.Box.pack_before]]
  * [[:develop:api:efl:ui:radio_box:method:pack_before|Efl.Ui.Radio_Box.pack_before]]
  * [[:develop:api:efl:ui:group_item:method:pack_before|Efl.Ui.Group_Item.pack_before]]
  * [[:develop:api:efl:ui:collection:method:pack_before|Efl.Ui.Collection.pack_before]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_before|Efl.Ui.Layout_Part_Box.pack_before]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_before|Efl.Canvas.Layout_Part_Invalid.pack_before]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_before|Efl.Ui.Spotlight.Container.pack_before]]
  * [[:develop:api:efl:ui:tab_pager:method:pack_before|Efl.Ui.Tab_Pager.pack_before]]

