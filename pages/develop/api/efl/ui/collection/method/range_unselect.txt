~~Title: Efl.Ui.Collection.range_unselect~~
====== Efl.Ui.Collection.range_unselect ======

===== Description =====

%%Unselect a range of %%[[:develop:api:efl:ui:selectable|Efl.Ui.Selectable]]%%.%%

%%This will unselect the range of selectables from a to b or from b to a depending on which one comes first. If a or b are not part of the widget, a error is returned, and no change is applied. %%''null''%% is not allowed as either of the parameters. Both of the passed values will also be unselected.%%
{{page>:develop:api-include:efl:ui:collection:method:range_unselect:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:multi_selectable:method:range_unselect|Efl.Ui.Multi_Selectable.range_unselect]].//===== Signature =====

<code>
range_unselect @beta @pure_virtual {
    params {
        @in a: Efl.Ui.Selectable;
        @in b: Efl.Ui.Selectable;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_selectable_range_unselect(Eo *obj, Efl_Ui_Selectable *a, Efl_Ui_Selectable *b);
</code>

===== Parameters =====

  * **a** //(in)// - %%One side of the range.%%
  * **b** //(in)// - %%The other side of the range.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:multi_selectable:method:range_unselect|Efl.Ui.Multi_Selectable.range_unselect]]
  * [[:develop:api:efl:ui:collection:method:range_unselect|Efl.Ui.Collection.range_unselect]]

