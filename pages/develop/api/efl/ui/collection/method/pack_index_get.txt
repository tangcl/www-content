~~Title: Efl.Ui.Collection.pack_index_get~~
====== Efl.Ui.Collection.pack_index_get ======

===== Description =====

%%Get the index of a sub-object in this container.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:collection:method:pack_index_get:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_linear:method:pack_index_get|Efl.Pack_Linear.pack_index_get]].//===== Signature =====

<code>
pack_index_get @pure_virtual {
    params {
        @in subobj: const(Efl.Gfx.Entity);
    }
    return: int (+1);
}
</code>

===== C signature =====

<code c>
int efl_pack_index_get(Eo *obj, const Efl_Gfx_Entity *subobj);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%An existing sub-object in this container.%%

===== Implemented by =====

  * [[:develop:api:efl:pack_linear:method:pack_index_get|Efl.Pack_Linear.pack_index_get]]
  * [[:develop:api:efl:ui:flip:method:pack_index_get|Efl.Ui.Flip.pack_index_get]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_index_get|Efl.Canvas.Layout_Part_Box.pack_index_get]]
  * [[:develop:api:efl:ui:box:method:pack_index_get|Efl.Ui.Box.pack_index_get]]
  * [[:develop:api:efl:ui:group_item:method:pack_index_get|Efl.Ui.Group_Item.pack_index_get]]
  * [[:develop:api:efl:ui:collection:method:pack_index_get|Efl.Ui.Collection.pack_index_get]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_index_get|Efl.Ui.Layout_Part_Box.pack_index_get]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_index_get|Efl.Canvas.Layout_Part_Invalid.pack_index_get]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_index_get|Efl.Ui.Spotlight.Container.pack_index_get]]

