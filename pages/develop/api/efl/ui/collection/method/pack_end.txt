~~Title: Efl.Ui.Collection.pack_end~~
====== Efl.Ui.Collection.pack_end ======

===== Description =====

%%Append object at the end of this container.%%

%%This is the same as %%[[:develop:api:efl:pack_linear:method:pack_at|Efl.Pack_Linear.pack_at]]%% with a %%''-1''%% index.%%

%%When this container is deleted, it will request deletion of the given %%''subobj''%%. Use %%[[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]%% to remove %%''subobj''%% from this container without deleting it.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:collection:method:pack_end:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_linear:method:pack_end|Efl.Pack_Linear.pack_end]].//===== Signature =====

<code>
pack_end @pure_virtual {
    params {
        @in subobj: Efl.Gfx.Entity;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_end(Eo *obj, Efl_Gfx_Entity *subobj);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%Object to pack at the end.%%

===== Implemented by =====

  * [[:develop:api:efl:pack_linear:method:pack_end|Efl.Pack_Linear.pack_end]]
  * [[:develop:api:efl:ui:tab_bar:method:pack_end|Efl.Ui.Tab_Bar.pack_end]]
  * [[:develop:api:efl:ui:flip:method:pack_end|Efl.Ui.Flip.pack_end]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_end|Efl.Canvas.Layout_Part_Box.pack_end]]
  * [[:develop:api:efl:ui:box:method:pack_end|Efl.Ui.Box.pack_end]]
  * [[:develop:api:efl:ui:radio_box:method:pack_end|Efl.Ui.Radio_Box.pack_end]]
  * [[:develop:api:efl:ui:group_item:method:pack_end|Efl.Ui.Group_Item.pack_end]]
  * [[:develop:api:efl:ui:collection:method:pack_end|Efl.Ui.Collection.pack_end]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_end|Efl.Ui.Layout_Part_Box.pack_end]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_end|Efl.Canvas.Layout_Part_Invalid.pack_end]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_end|Efl.Ui.Spotlight.Container.pack_end]]
  * [[:develop:api:efl:ui:tab_pager:method:pack_end|Efl.Ui.Tab_Pager.pack_end]]

