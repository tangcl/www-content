~~Title: Efl.Ui.Widget_Factory~~
====== Efl.Ui.Widget_Factory (class) ======

===== Description =====

%%Efl Ui Factory that provides %%[[:develop:api:efl:ui:widget|Efl.Ui.Widget]]%%.%%

%%This factory is designed to build %%[[:develop:api:efl:ui:widget|Efl.Ui.Widget]]%% and optionally set their %%[[:develop:api:efl:ui:widget:property:style|Efl.Ui.Widget.style]]%% if it was connected with %%[[:develop:api:efl:ui:property_bind:method:property_bind|Efl.Ui.Property_Bind.property_bind]]%% "%%''style''%%".%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:widget_factory:description&nouser&nolink&nodate}}

===== Inheritance =====

 => [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] //(class)// => [[:develop:api:efl:object|Efl.Object]] //(class)//
++++ Full hierarchy |

  * [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] //(class)//
    * [[:develop:api:efl:object|Efl.Object]] //(class)//
  * [[:develop:api:efl:ui:factory|Efl.Ui.Factory]] //(interface)//
    * [[:develop:api:efl:ui:property_bind|Efl.Ui.Property_Bind]] //(interface)//
    * [[:develop:api:efl:ui:factory_bind|Efl.Ui.Factory_Bind]] //(interface)//
  * [[:develop:api:efl:part|Efl.Part]] //(interface)//


++++
===== Members =====

**[[:develop:api:efl:ui:widget_factory:method:constructor|constructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to provide optional initialization code for your object.%%
<code c>
Efl_Object *efl_constructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:widget_factory:method:factory_bind|factory_bind]]**// [Overridden from [[:develop:api:efl:ui:factory_bind|Efl.Ui.Factory_Bind]]]//\\
> %%bind the factory with the given key string. when the data is ready or changed, factory create the object and bind the data to the key action and process promised work. Note: the input %%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%% need to be %%[[:develop:api:efl:ui:property_bind:method:property_bind|Efl.Ui.Property_Bind.property_bind]]%% at least once.%%
<code c>
Eina_Error efl_ui_factory_bind(Eo *obj, const char *key, Efl_Ui_Factory *factory);
</code>
\\
**[[:develop:api:efl:ui:widget_factory:method:finalize|finalize]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to finish the initialization of your object after all (if any) user-provided configuration methods have been executed.%%
<code c>
Efl_Object *efl_finalize(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:widget_factory:property:item_class|item_class]]** //**(get, set)**//\\
> %%Define the class of the item returned by this factory.%%
<code c>
const Efl_Class *efl_ui_widget_factory_item_class_get(const Eo *obj);
void efl_ui_widget_factory_item_class_set(Eo *obj, const Efl_Class *klass);
</code>
\\
**[[:develop:api:efl:ui:widget_factory:method:property_bind|property_bind]]**// [Overridden from [[:develop:api:efl:ui:property_bind|Efl.Ui.Property_Bind]]]//\\
> %%bind property data with the given key string. when the data is ready or changed, bind the data to the key action and process promised work.%%
<code c>
Eina_Error efl_ui_property_bind(Eo *obj, const char *key, const char *property);
</code>
\\
**[[:develop:api:efl:ui:widget_factory:method:release|release]]**// [Overridden from [[:develop:api:efl:ui:factory|Efl.Ui.Factory]]]//\\
> %%Release a UI object and disconnect from models.%%
<code c>
void efl_ui_factory_release(Eo *obj, Eina_Iterator *ui_views);
</code>
\\
**[[:develop:api:efl:ui:widget_factory:method:create|create]]** ''protected''// [Overridden from [[:develop:api:efl:ui:factory|Efl.Ui.Factory]]]//\\
> %%Create a UI object from the necessary properties in the specified model.%%
<code c>
Eina_Future *efl_ui_factory_create(Eo *obj, Eina_Iterator *models);
</code>
\\
**[[:develop:api:efl:ui:widget_factory:method:part_get|part_get]]** ''protected''// [Overridden from [[:develop:api:efl:part|Efl.Part]]]//\\
> %%Get a proxy object referring to a part of an object.%%
<code c>
Efl_Object *efl_part_get(const Eo *obj, const char *name);
</code>
\\

==== Inherited ====

^ [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] ^^^
|  | **[[:develop:api:efl:loop_consumer:method:future_rejected|future_rejected]]** | %%Creates a new future that is already rejected to a specified error using the %%[[:develop:api:efl:loop_consumer:property:loop|Efl.Loop_Consumer.loop.get]]%%.%% |
|  | **[[:develop:api:efl:loop_consumer:method:future_resolved|future_resolved]]** | %%Creates a new future that is already resolved to a value.%% |
|  | **[[:develop:api:efl:loop_consumer:property:loop|loop]]** //**(get)**// |  |
|  | **[[:develop:api:efl:loop_consumer:property:parent|parent]]** //**(get, set)**// | %%The parent of an object.%% |
|  | **[[:develop:api:efl:loop_consumer:method:promise_new|promise_new]]** | %%Create a new promise with the scheduler coming from the loop provided by this object.%% |
^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:property:allow_parent_unref|allow_parent_unref]]** //**(get, set)**// | %%Allow an object to be deleted by unref even if it has a parent.%% |
|  | **[[:develop:api:efl:object:method:children_iterator_new|children_iterator_new]]** | %%Get an iterator on all children.%% |
|  | **[[:develop:api:efl:object:property:comment|comment]]** //**(get, set)**// | %%A human readable comment for the object.%% |
|  | **[[:develop:api:efl:object:method:composite_attach|composite_attach]]** | %%Make an object a composite object of another.%% |
|  | **[[:develop:api:efl:object:method:composite_detach|composite_detach]]** | %%Detach a composite object from another object.%% |
|  | **[[:develop:api:efl:object:method:composite_part_is|composite_part_is]]** | %%Check if an object is part of a composite object.%% |
|  | **[[:develop:api:efl:object:method:debug_name_override|debug_name_override]]** | %%Build a read-only name for this object used for debugging.%% |
|  | **[[:develop:api:efl:object:method:destructor|destructor]]** | %%Implement this method to provide deinitialization code for your object if you need it.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_del|event_callback_forwarder_del]]** | %%Remove an event callback forwarder for a specified event and object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_priority_add|event_callback_forwarder_priority_add]]** | %%Add an event callback forwarder that will make this object emit an event whenever another object (%%''source''%%) emits it. The event is said to be forwarded from %%''source''%% to this object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_stop|event_callback_stop]]** | %%Stop the current callback call.%% |
|  | **[[:develop:api:efl:object:method:event_freeze|event_freeze]]** | %%Freeze events of this object.%% |
|  | **[[:develop:api:efl:object:property:event_freeze_count|event_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_freeze|event_global_freeze]]** | %%Globally freeze events for ALL EFL OBJECTS.%% |
|  ''static'' | **[[:develop:api:efl:object:property:event_global_freeze_count|event_global_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_thaw|event_global_thaw]]** | %%Globally thaw events for ALL EFL OBJECTS.%% |
|  | **[[:develop:api:efl:object:method:event_thaw|event_thaw]]** | %%Thaw events of object.%% |
|  | **[[:develop:api:efl:object:property:finalized|finalized]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:method:invalidate|invalidate]]** | %%Implement this method to perform special actions when your object loses its parent, if you need to.%% |
|  | **[[:develop:api:efl:object:property:invalidated|invalidated]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidating|invalidating]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:name|name]]** //**(get, set)**// | %%The name of the object.%% |
|  | **[[:develop:api:efl:object:method:name_find|name_find]]** | %%Find a child object with the given name and return it.%% |
|  | **[[:develop:api:efl:object:method:provider_find|provider_find]]** | %%Searches upwards in the object tree for a provider which knows the given class/interface.%% |
|  | **[[:develop:api:efl:object:method:provider_register|provider_register]]** | %%Will register a manager of a specific class to be answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
|  | **[[:develop:api:efl:object:method:provider_unregister|provider_unregister]]** | %%Will unregister a manager of a specific class that was previously registered and answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |

===== Events =====

==== Inherited ====

^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:event:del|del]]** | %%Object is being deleted. See %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%.%% |
|  | **[[:develop:api:efl:object:event:destruct|destruct]]** | %%Object has been fully destroyed. It can not be used beyond this point. This event should only serve to clean up any reference you keep to the object.%% |
|  | **[[:develop:api:efl:object:event:invalidate|invalidate]]** | %%Object is being invalidated and losing its parent. See %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%%.%% |
|  | **[[:develop:api:efl:object:event:noref|noref]]** | %%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%% |
|  | **[[:develop:api:efl:object:event:ownership_shared|ownership,shared]]** | %%Object has acquired a second reference. It has multiple owners now. Triggered whenever increasing the refcount from one to two, it will not trigger by further increasing the refcount beyond two.%% |
|  | **[[:develop:api:efl:object:event:ownership_unique|ownership,unique]]** | %%Object has lost a reference and only one is left. It has just one owner now. Triggered whenever the refcount goes from two to one.%% |
^ [[:develop:api:efl:ui:factory|Efl.Ui.Factory]] ^^^
|  | **[[:develop:api:efl:ui:factory:event:item_building|item,building]]** | %%Event emitted when an item has processed %%[[:develop:api:efl:object:method:finalize|Efl.Object.finalize]]%%, but before all the factory are done building it. Note: If the %%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%% keeps a cache of objects, this will be called when objects are pulled from the cache.%% |
|  | **[[:develop:api:efl:ui:factory:event:item_constructing|item,constructing]]** | %%Event emitted when an item is under construction (between the %%[[:develop:api:efl:object:method:constructor|Efl.Object.constructor]]%% and %%[[:develop:api:efl:object:method:finalize|Efl.Object.finalize]]%% call on the item). Note: If the %%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%% keeps a cache of objects, this won't be called when objects are pulled from the cache.%% |
|  | **[[:develop:api:efl:ui:factory:event:item_created|item,created]]** | %%Event emitted when an item has been successfully created by the factory and is about to be used by an %%[[:develop:api:efl:ui:view|Efl.Ui.View]]%%.%% |
|  | **[[:develop:api:efl:ui:factory:event:item_releasing|item,releasing]]** | %%Event emitted when an item is being released by the %%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%%. It must be assumed that after this call, the object can be recycles to another %%[[:develop:api:efl:ui:view|Efl.Ui.View]]%% and there can be more than one call for the same item.%% |
^ [[:develop:api:efl:ui:property_bind|Efl.Ui.Property_Bind]] ^^^
|  | **[[:develop:api:efl:ui:property_bind:event:properties_changed|properties,changed]]** | %%Event dispatched when a property on the object has changed due to a user interaction on the object that a model could be interested in.%% |
|  | **[[:develop:api:efl:ui:property_bind:event:property_bound|property,bound]]** | %%Event dispatched when a property on the object is bound to a model. This is useful to avoid generating too many events.%% |
