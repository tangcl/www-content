~~Title: Efl.Ui.Focus.Manager.request_subchild~~
====== Efl.Ui.Focus.Manager.request_subchild ======

===== Description =====

%%Returns the widget in the direction next.%%

%%The returned widget is a child of %%''root''%%. It's guaranteed that child will not be prepared again, so you can call this function inside a %%[[:develop:api:efl:ui:focus:object:method:setup_order|Efl.Ui.Focus.Object.setup_order]]%% call.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:focus:manager:method:request_subchild:description&nouser&nolink&nodate}}

===== Signature =====

<code>
request_subchild @pure_virtual {
    params {
        @in root: Efl.Ui.Focus.Object;
    }
    return: Efl.Ui.Focus.Object;
}
</code>

===== C signature =====

<code c>
Efl_Ui_Focus_Object *efl_ui_focus_manager_request_subchild(Eo *obj, Efl_Ui_Focus_Object *root);
</code>

===== Parameters =====

  * **root** //(in)// - %%Parent for returned child.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:manager:method:request_subchild|Efl.Ui.Focus.Manager.request_subchild]]
  * [[:develop:api:efl:ui:focus:manager_calc:method:request_subchild|Efl.Ui.Focus.Manager_Calc.request_subchild]]

