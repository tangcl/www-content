~~Title: Efl.Ui.Focus.Object: focus_parent,changed~~

===== Description =====

%%Emitted when a new logical parent should be used.%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:focus:object:event:focus_parent_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
focus_parent,changed: Efl.Ui.Focus.Object;
</code>

===== C information =====

<code c>
EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_PARENT_CHANGED(Efl_Ui_Focus_Object *)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_focus_object_event_focus_parent_changed(void *data, const Efl_Event *event)
{
    Efl_Ui_Focus_Object *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_PARENT_CHANGED, on_efl_ui_focus_object_event_focus_parent_changed, d);
}

</code>
