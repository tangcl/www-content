~~Title: Efl.Ui.Focus.Manager_Logical_End_Detail~~

===== Description =====

%%Structure holding the focus object with extra information on logical end.%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:focus:manager_logical_end_detail:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:ui:focus:manager_logical_end_detail:fields&nouser&nolink&nodate}}

  * **is_regular_end** - %%%%''true''%% if element is registered as regular element in the %%[[:develop:api:efl:ui:focus:manager|Efl.Ui.Focus.Manager]]%% object.%%
  * **element** - %%The last element of the logical chain in the %%[[:develop:api:efl:ui:focus:manager|Efl.Ui.Focus.Manager]]%%.%%

===== Signature =====

<code>
struct Efl.Ui.Focus.Manager_Logical_End_Detail {
    is_regular_end: bool;
    element: Efl.Ui.Focus.Object;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Ui_Focus_Manager_Logical_End_Detail {
    Eina_Bool is_regular_end;
    Efl_Ui_Focus_Object *element;
} Efl_Ui_Focus_Manager_Logical_End_Detail;
</code>
