~~Title: Efl.Ui.Image.signal_emit~~
====== Efl.Ui.Image.signal_emit ======

===== Description =====

%%Sends/emits an Edje signal to this layout.%%

%%This function sends a signal to the object. An Edje program, at the EDC specification level, can respond to a signal by having declared matching "signal" and "source" fields on its block.%%

%%See also the Edje Data Collection Reference for EDC files.%%

%%See %%[[:develop:api:efl:layout:signal:method:signal_callback_add|Efl.Layout.Signal.signal_callback_add]]%%() for more on Edje signals.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:image:method:signal_emit:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:layout:signal:method:signal_emit|Efl.Layout.Signal.signal_emit]].//===== Signature =====

<code>
signal_emit @pure_virtual {
    params {
        @in emission: string;
        @in source: string;
    }
}
</code>

===== C signature =====

<code c>
void efl_layout_signal_emit(Eo *obj, const char *emission, const char *source);
</code>

===== Parameters =====

  * **emission** //(in)// - %%The signal's "emission" string%%
  * **source** //(in)// - %%The signal's "source" string%%

===== Implemented by =====

  * [[:develop:api:efl:layout:signal:method:signal_emit|Efl.Layout.Signal.signal_emit]]
  * [[:develop:api:efl:canvas:layout:method:signal_emit|Efl.Canvas.Layout.signal_emit]]
  * [[:develop:api:efl:ui:image:method:signal_emit|Efl.Ui.Image.signal_emit]]
  * [[:develop:api:efl:ui:layout_base:method:signal_emit|Efl.Ui.Layout_Base.signal_emit]]
  * [[:develop:api:efl:ui:text:method:signal_emit|Efl.Ui.Text.signal_emit]]

