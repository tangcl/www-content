~~Title: Efl.Ui.Autorepeat: repeated~~

===== Description =====

%%Called when a repeated event is emitted%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:autorepeat:event:repeated:description&nouser&nolink&nodate}}

===== Signature =====

<code>
repeated;
</code>

===== C information =====

<code c>
EFL_UI_AUTOREPEAT_EVENT_REPEATED(void)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_autorepeat_event_repeated(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_AUTOREPEAT_EVENT_REPEATED, on_efl_ui_autorepeat_event_repeated, d);
}

</code>
