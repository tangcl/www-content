~~Title: Efl.Ui.Autorepeat.autorepeat_enabled~~
====== Efl.Ui.Autorepeat.autorepeat_enabled ======

===== Description =====

%%Turn on/off the autorepeat event generated when a button is kept pressed.%%

%%When off, no autorepeat is performed and buttons emit a normal %%''clicked''%% event when they are clicked.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:autorepeat:property:autorepeat_enabled:description&nouser&nolink&nodate}}

===== Values =====

  * **on** - %%A bool to turn on/off the repeat event generation.%%

===== Signature =====

<code>
@property autorepeat_enabled @pure_virtual {
    get {}
    set {}
    values {
        on: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_autorepeat_enabled_get(const Eo *obj);
void efl_ui_autorepeat_enabled_set(Eo *obj, Eina_Bool on);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:autorepeat:property:autorepeat_enabled|Efl.Ui.Autorepeat.autorepeat_enabled]]
  * [[:develop:api:efl:ui:button:property:autorepeat_enabled|Efl.Ui.Button.autorepeat_enabled]]

