~~Title: Efl.Ui.Win.object_top_in_rectangle_get~~
====== Efl.Ui.Win.object_top_in_rectangle_get ======

===== Description =====

%%Retrieve the canvas object stacked at the top of a given rectangular region in a canvas%%

%%This function will traverse all the layers of the given canvas, from top to bottom, querying for objects with areas overlapping with the given rectangular region. The user can exclude from the query objects which are hidden and/or which are set to pass events.%%

<note warning>
%%This function will only evaluate top-level objects; child or "sub" objects will be skipped.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:method:object_top_in_rectangle_get:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:canvas:scene:method:object_top_in_rectangle_get|Efl.Canvas.Scene.object_top_in_rectangle_get]].//===== Signature =====

<code>
object_top_in_rectangle_get @const @pure_virtual {
    params {
        @in rect: Eina.Rect;
        @in include_pass_events_objects: bool;
        @in include_hidden_objects: bool;
    }
    return: Efl.Gfx.Entity @no_unused;
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Entity *efl_canvas_scene_object_top_in_rectangle_get(const Eo *obj, Eina_Rect rect, Eina_Bool include_pass_events_objects, Eina_Bool include_hidden_objects);
</code>

===== Parameters =====

  * **rect** //(in)// - %%The rectangular region.%%
  * **include_pass_events_objects** //(in)// - %%Boolean flag to include or not objects which pass events in this calculation.%%
  * **include_hidden_objects** //(in)// - %%Boolean flag to include or not hidden objects in this calculation.%%

===== Implemented by =====

  * [[:develop:api:efl:canvas:scene:method:object_top_in_rectangle_get|Efl.Canvas.Scene.object_top_in_rectangle_get]]
  * [[:develop:api:efl:ui:win:method:object_top_in_rectangle_get|Efl.Ui.Win.object_top_in_rectangle_get]]

