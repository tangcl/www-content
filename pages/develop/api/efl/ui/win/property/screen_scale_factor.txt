~~Title: Efl.Ui.Win.screen_scale_factor~~
====== Efl.Ui.Win.screen_scale_factor ======

===== Values =====

  * **size** - %%The screen scaling factor.%%


\\ {{page>:develop:api-include:efl:ui:win:property:screen_scale_factor:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:screen:property:screen_scale_factor|Efl.Screen.screen_scale_factor]] **(get)**.//===== Signature =====

<code>
@property screen_scale_factor @pure_virtual {
    get {}
    values {
        size: float;
    }
}
</code>

===== C signature =====

<code c>
float efl_screen_scale_factor_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:screen:property:screen_scale_factor|Efl.Screen.screen_scale_factor]]
  * [[:develop:api:efl:ui:win:property:screen_scale_factor|Efl.Ui.Win.screen_scale_factor]]

