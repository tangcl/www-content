~~Title: Efl.Ui.Win.win_rotation~~
====== Efl.Ui.Win.win_rotation ======

===== Description =====

%%The rotation of this window%%

%%The value will automatically change when the Window Manager of this window changes its rotation. This rotation is automatically applied to all %%[[:develop:api:efl:ui:layout|Efl.Ui.Layout]]%% objects.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:win_rotation:description&nouser&nolink&nodate}}

===== Values =====

  * **rotation** - %%The rotation of the window%%

===== Signature =====

<code>
@property win_rotation @beta {
    get {}
    set {}
    values {
        rotation: int;
    }
}
</code>

===== C signature =====

<code c>
int efl_ui_win_rotation_get(const Eo *obj);
void efl_ui_win_rotation_set(Eo *obj, int rotation);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:win_rotation|Efl.Ui.Win.win_rotation]]

