~~Title: Efl.Ui.Win.focus_highlight_enabled~~
====== Efl.Ui.Win.focus_highlight_enabled ======

===== Description =====

%%Whether focus highlight is enabled or not on this window, regardless of the global setting.%%

%%See also %%[[:develop:api:efl:ui:win:property:focus_highlight_style|Efl.Ui.Win.focus_highlight_style]]%%. See also %%[[:develop:api:efl:ui:win:property:focus_highlight_animate|Efl.Ui.Win.focus_highlight_animate]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:focus_highlight_enabled:description&nouser&nolink&nodate}}

===== Values =====

  * **enabled** - %%The enabled value for the highlight.%%

===== Signature =====

<code>
@property focus_highlight_enabled {
    get {}
    set {}
    values {
        enabled: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_win_focus_highlight_enabled_get(const Eo *obj);
void efl_ui_win_focus_highlight_enabled_set(Eo *obj, Eina_Bool enabled);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:focus_highlight_enabled|Efl.Ui.Win.focus_highlight_enabled]]

