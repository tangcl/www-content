~~Title: Efl.Ui.Win.hint_aspect~~
====== Efl.Ui.Win.hint_aspect ======

===== Description =====

%%Defines the aspect ratio to respect when scaling this object.%%

%%The aspect ratio is defined as the width / height ratio of the object. Depending on the object and its container, this hint may or may not be fully respected.%%

%%If any of the given aspect ratio terms are 0, the object's container will ignore the aspect and scale this object to occupy the whole available area, for any given policy.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:hint_aspect:description&nouser&nolink&nodate}}

===== Values =====

  * **mode** - %%Mode of interpretation.%%
  * **sz** - %%Base size to use for aspecting.%%

//Overridden from [[:develop:api:efl:canvas:object:property:hint_aspect|Efl.Gfx.Hint.hint_aspect]] **(set)**.//===== Signature =====

<code>
@property hint_aspect @pure_virtual {
    get {}
    set {}
    values {
        mode: Efl.Gfx.Hint_Aspect;
        sz: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
void efl_gfx_hint_aspect_get(const Eo *obj, Efl_Gfx_Hint_Aspect *mode, Eina_Size2D *sz);
void efl_gfx_hint_aspect_set(Eo *obj, Efl_Gfx_Hint_Aspect mode, Eina_Size2D sz);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:hint:property:hint_aspect|Efl.Gfx.Hint.hint_aspect]]
  * [[:develop:api:efl:canvas:object:property:hint_aspect|Efl.Canvas.Object.hint_aspect]]
  * [[:develop:api:efl:ui:win:property:hint_aspect|Efl.Ui.Win.hint_aspect]]

