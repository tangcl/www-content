~~Title: Efl.Ui.Win: win_rotation,changed~~

===== Description =====

%%Called when window rotation is changed, sends current rotation in degrees%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:win:event:win_rotation_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
win_rotation,changed: int;
</code>

===== C information =====

<code c>
EFL_UI_WIN_EVENT_WIN_ROTATION_CHANGED(int)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_win_event_win_rotation_changed(void *data, const Efl_Event *event)
{
    int info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_WIN_EVENT_WIN_ROTATION_CHANGED, on_efl_ui_win_event_win_rotation_changed, d);
}

</code>
