~~Title: Efl.Ui.Box.layout_update~~
====== Efl.Ui.Box.layout_update ======

===== Description =====

%%Implementation of this container's layout algorithm.%%

%%EFL will call this function whenever the contents of this container need to be re-laid out on the canvas.%%

%%This can be overridden to implement custom layout behaviors.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:box:method:layout_update:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_layout:method:layout_update|Efl.Pack_Layout.layout_update]].//===== Signature =====

<code>
layout_update @protected @pure_virtual {}
</code>

===== C signature =====

<code c>
void efl_pack_layout_update(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:pack_layout:method:layout_update|Efl.Pack_Layout.layout_update]]
  * [[:develop:api:efl:ui:table:method:layout_update|Efl.Ui.Table.layout_update]]
  * [[:develop:api:efl:ui:table_static:method:layout_update|Efl.Ui.Table_Static.layout_update]]
  * [[:develop:api:efl:ui:box:method:layout_update|Efl.Ui.Box.layout_update]]
  * [[:develop:api:efl:ui:box_stack:method:layout_update|Efl.Ui.Box_Stack.layout_update]]
  * [[:develop:api:efl:ui:box_flow:method:layout_update|Efl.Ui.Box_Flow.layout_update]]
  * [[:develop:api:efl:ui:relative_layout:method:layout_update|Efl.Ui.Relative_Layout.layout_update]]

