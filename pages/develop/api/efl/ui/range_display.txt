~~Title: Efl.Ui.Range_Display~~
====== Efl.Ui.Range_Display (interface) ======

===== Description =====

%%Interface that contains properties regarding the displaying of a value within a range.%%

%%A value range contains a value restricted between specified minimum and maximum limits at all times. This can be used for progressbars, sliders or spinners, for example.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:range_display:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:ui:range_display:property:range_limits|range_limits]]** //**(get, set)**//\\
> %%Set the minimum and maximum values for given range widget.%%
<code c>
void efl_ui_range_limits_get(const Eo *obj, double *min, double *max);
void efl_ui_range_limits_set(Eo *obj, double min, double max);
</code>
\\
**[[:develop:api:efl:ui:range_display:property:range_value|range_value]]** //**(get, set)**//\\
> %%Control the value (position) of the widget within its valid range.%%
<code c>
double efl_ui_range_value_get(const Eo *obj);
void efl_ui_range_value_set(Eo *obj, double val);
</code>
\\

===== Events =====

**[[:develop:api:efl:ui:range_display:event:changed|changed]]**\\
> %%Emitted when the %%[[:develop:api:efl:ui:range_display:property:range_value|Efl.Ui.Range_Display.range_value]]%% is getting changed.%%
<code c>
EFL_UI_RANGE_EVENT_CHANGED(void)
</code>
\\ **[[:develop:api:efl:ui:range_display:event:max_reached|max,reached]]**\\
> %%Emitted when the %%''range_value''%% has reached the maximum of %%[[:develop:api:efl:ui:range_display:property:range_limits|Efl.Ui.Range_Display.range_limits]]%%.%%
<code c>
EFL_UI_RANGE_EVENT_MAX_REACHED(void)
</code>
\\ **[[:develop:api:efl:ui:range_display:event:min_reached|min,reached]]**\\
> %%Emitted when the %%[[:develop:api:efl:ui:range_display:property:range_value|Efl.Ui.Range_Display.range_value]]%% has reached the minimum of %%[[:develop:api:efl:ui:range_display:property:range_limits|Efl.Ui.Range_Display.range_limits]]%%.%%
<code c>
EFL_UI_RANGE_EVENT_MIN_REACHED(void)
</code>
\\ 