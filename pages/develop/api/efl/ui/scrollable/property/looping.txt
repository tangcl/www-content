~~Title: Efl.Ui.Scrollable.looping~~
====== Efl.Ui.Scrollable.looping ======

===== Description =====

%%Controls infinite looping for a scroller.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:scrollable:property:looping:description&nouser&nolink&nodate}}

===== Values =====

  * **loop_h** - %%Scroll horizontal looping is enabled.%%
  * **loop_v** - %%Scroll vertical looping is enabled.%%

===== Signature =====

<code>
@property looping @pure_virtual {
    get {}
    set {}
    values {
        loop_h: bool;
        loop_v: bool;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_scrollable_looping_get(const Eo *obj, Eina_Bool *loop_h, Eina_Bool *loop_v);
void efl_ui_scrollable_looping_set(Eo *obj, Eina_Bool loop_h, Eina_Bool loop_v);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:property:looping|Efl.Ui.Scrollable.looping]]
  * [[:develop:api:efl:ui:scroll:manager:property:looping|Efl.Ui.Scroll.Manager.looping]]

