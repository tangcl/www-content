~~Title: Efl.Ui.Scrollable.content_size~~
====== Efl.Ui.Scrollable.content_size ======

===== Values =====

  * **size** - %%The content size in pixels.%%


\\ {{page>:develop:api-include:efl:ui:scrollable:property:content_size:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property content_size @pure_virtual {
    get {}
    values {
        size: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_ui_scrollable_content_size_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:property:content_size|Efl.Ui.Scrollable.content_size]]
  * [[:develop:api:efl:ui:scroll:manager:property:content_size|Efl.Ui.Scroll.Manager.content_size]]

