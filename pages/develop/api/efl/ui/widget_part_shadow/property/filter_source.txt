~~Title: Efl.Ui.Widget_Part_Shadow.filter_source~~
====== Efl.Ui.Widget_Part_Shadow.filter_source ======

===== Description =====

%%Bind an object to use as a mask or texture in a filter program.%%

%%This will create automatically a new RGBA buffer containing the source object's pixels (as it is rendered).%%
{{page>:develop:api-include:efl:ui:widget_part_shadow:property:filter_source:description&nouser&nolink&nodate}}

===== Keys =====

  * **name** - %%Buffer name as used in the program.%%
===== Values =====

  * **source** - %%Object to use as a source of pixels.%%

//Overridden from [[:develop:api:efl:gfx:filter:property:filter_source|Efl.Gfx.Filter.filter_source]] **(get, set)**.//===== Signature =====

<code>
@property filter_source @pure_virtual {
    get {}
    set {}
    keys {
        name: string;
    }
    values {
        source: Efl.Gfx.Entity;
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Entity *efl_gfx_filter_source_get(const Eo *obj, const char *name);
void efl_gfx_filter_source_set(Eo *obj, const char *name, Efl_Gfx_Entity *source);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:filter:property:filter_source|Efl.Gfx.Filter.filter_source]]
  * [[:develop:api:efl:ui:widget_part_shadow:property:filter_source|Efl.Ui.Widget_Part_Shadow.filter_source]]
  * [[:develop:api:efl:canvas:filter:internal:property:filter_source|Efl.Canvas.Filter.Internal.filter_source]]
  * [[:develop:api:efl:canvas:text:property:filter_source|Efl.Canvas.Text.filter_source]]

