~~Title: Efl.Ui.Scrollbar: bar,dragged~~

===== Description =====

%%Emitted when thumb is dragged.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:scrollbar:event:bar_dragged:description&nouser&nolink&nodate}}

===== Signature =====

<code>
bar,dragged: Efl.Ui.Layout_Orientation;
</code>

===== C information =====

<code c>
EFL_UI_SCROLLBAR_EVENT_BAR_DRAGGED(Efl_Ui_Layout_Orientation)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_scrollbar_event_bar_dragged(void *data, const Efl_Event *event)
{
    Efl_Ui_Layout_Orientation info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_SCROLLBAR_EVENT_BAR_DRAGGED, on_efl_ui_scrollbar_event_bar_dragged, d);
}

</code>
