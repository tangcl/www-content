~~Title: Efl.Ui.Layout_Base.group_data~~
====== Efl.Ui.Layout_Base.group_data ======

===== Keys =====

  * **key** - %%The data field's key string%%
===== Values =====

  * **val** - %%The data's value string.%%


\\ {{page>:develop:api-include:efl:ui:layout_base:property:group_data:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:layout:group:property:group_data|Efl.Layout.Group.group_data]] **(get)**.//===== Signature =====

<code>
@property group_data @pure_virtual {
    get {
        keys {
            key: string;
        }
    }
    values {
        val: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_layout_group_data_get(const Eo *obj, const char *key);
</code>

===== Implemented by =====

  * [[:develop:api:efl:layout:group:property:group_data|Efl.Layout.Group.group_data]]
  * [[:develop:api:efl:canvas:layout:property:group_data|Efl.Canvas.Layout.group_data]]
  * [[:develop:api:efl:ui:image:property:group_data|Efl.Ui.Image.group_data]]
  * [[:develop:api:efl:ui:layout_base:property:group_data|Efl.Ui.Layout_Base.group_data]]

