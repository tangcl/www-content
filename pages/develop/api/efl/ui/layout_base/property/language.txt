~~Title: Efl.Ui.Layout_Base.language~~
====== Efl.Ui.Layout_Base.language ======

===== Description =====

%%The (human) language for this object.%%
{{page>:develop:api-include:efl:ui:layout_base:property:language:description&nouser&nolink&nodate}}

===== Values =====

  * **language** - %%The current language.%%

//Overridden from [[:develop:api:efl:ui:i18n:property:language|Efl.Ui.I18n.language]] **(get, set)**.//===== Signature =====

<code>
@property language @pure_virtual {
    get {}
    set {}
    values {
        language: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_ui_language_get(const Eo *obj);
void efl_ui_language_set(Eo *obj, const char *language);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:i18n:property:language|Efl.Ui.I18n.language]]
  * [[:develop:api:efl:canvas:layout:property:language|Efl.Canvas.Layout.language]]
  * [[:develop:api:efl:ui:win:property:language|Efl.Ui.Win.language]]
  * [[:develop:api:efl:ui:table:property:language|Efl.Ui.Table.language]]
  * [[:develop:api:efl:ui:box:property:language|Efl.Ui.Box.language]]
  * [[:develop:api:efl:ui:layout_base:property:language|Efl.Ui.Layout_Base.language]]

