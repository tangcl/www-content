~~Title: Efl.Ui.Spin_Button.on_focus_update~~
====== Efl.Ui.Spin_Button.on_focus_update ======

===== Description =====

%%Virtual function handling focus in/out events on the widget.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:spin_button:method:on_focus_update:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:layout_base:method:on_focus_update|Efl.Ui.Focus.Object.on_focus_update]].//===== Signature =====

<code>
on_focus_update @protected {
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_focus_object_on_focus_update(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:object:method:on_focus_update|Efl.Ui.Focus.Object.on_focus_update]]
  * [[:develop:api:efl:ui:widget:method:on_focus_update|Efl.Ui.Widget.on_focus_update]]
  * [[:develop:api:efl:ui:win:method:on_focus_update|Efl.Ui.Win.on_focus_update]]
  * [[:develop:api:efl:ui:image_zoomable:method:on_focus_update|Efl.Ui.Image_Zoomable.on_focus_update]]
  * [[:develop:api:efl:ui:layout_base:method:on_focus_update|Efl.Ui.Layout_Base.on_focus_update]]
  * [[:develop:api:efl:ui:slider:method:on_focus_update|Efl.Ui.Slider.on_focus_update]]
  * [[:develop:api:efl:ui:text:method:on_focus_update|Efl.Ui.Text.on_focus_update]]
  * [[:develop:api:efl:ui:spin_button:method:on_focus_update|Efl.Ui.Spin_Button.on_focus_update]]
  * [[:develop:api:efl:ui:calendar:method:on_focus_update|Efl.Ui.Calendar.on_focus_update]]

