~~Title: Efl.Ui.Spin_Button.apply_formatted_value~~
====== Efl.Ui.Spin_Button.apply_formatted_value ======

===== Description =====

%%Internal method to be implemented by widgets including this mixin.%%

%%The mixin will call this method to signal the widget that the formatting has changed and therefore the current value should be converted and rendered again. Widgets must typically call %%[[:develop:api:efl:ui:format:method:formatted_value_get|Efl.Ui.Format.formatted_value_get]]%% and display the returned string. This is something they are already doing (whenever the value changes, for example) so there should be no extra code written to implement this method.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:spin_button:method:apply_formatted_value:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:spin:method:apply_formatted_value|Efl.Ui.Format.apply_formatted_value]].//===== Signature =====

<code>
apply_formatted_value @protected @pure_virtual {}
</code>

===== C signature =====

<code c>
void efl_ui_format_apply_formatted_value(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:format:method:apply_formatted_value|Efl.Ui.Format.apply_formatted_value]]
  * [[:develop:api:efl:ui:spin:method:apply_formatted_value|Efl.Ui.Spin.apply_formatted_value]]
  * [[:develop:api:efl:ui:spin_button:method:apply_formatted_value|Efl.Ui.Spin_Button.apply_formatted_value]]
  * [[:develop:api:efl:ui:tags:method:apply_formatted_value|Efl.Ui.Tags.apply_formatted_value]]
  * [[:develop:api:efl:ui:calendar:method:apply_formatted_value|Efl.Ui.Calendar.apply_formatted_value]]
  * [[:develop:api:efl:ui:progressbar:method:apply_formatted_value|Efl.Ui.Progressbar.apply_formatted_value]]

