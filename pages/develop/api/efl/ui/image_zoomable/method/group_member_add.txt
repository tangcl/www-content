~~Title: Efl.Ui.Image_Zoomable.group_member_add~~
====== Efl.Ui.Image_Zoomable.group_member_add ======

===== Description =====

%%Set a canvas object as a member of a given group (or smart object).%%

%%Members will automatically be stacked and layered together with the smart object. The various stacking functions will operate on members relative to the other members instead of the entire canvas, since they now live on an exclusive layer (see %%[[:develop:api:efl:gfx:stack:method:stack_above|Efl.Gfx.Stack.stack_above]]%%(), for more details).%%

%%Subclasses inheriting from this one may override this function to ensure the proper stacking of special objects, such as clippers, event rectangles, etc...%%

%%See also %%[[:develop:api:efl:canvas:group:method:group_member_remove|Efl.Canvas.Group.group_member_remove]]%%. See also %%[[:develop:api:efl:canvas:group:method:group_member_is|Efl.Canvas.Group.group_member_is]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:image_zoomable:method:group_member_add:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:image:method:group_member_add|Efl.Canvas.Group.group_member_add]].//===== Signature =====

<code>
group_member_add {
    params {
        @in sub_obj: Efl.Canvas.Object;
    }
}
</code>

===== C signature =====

<code c>
void efl_canvas_group_member_add(Eo *obj, Efl_Canvas_Object *sub_obj);
</code>

===== Parameters =====

  * **sub_obj** //(in)// - %%The member object.%%

===== Implemented by =====

  * [[:develop:api:efl:canvas:group:method:group_member_add|Efl.Canvas.Group.group_member_add]]
  * [[:develop:api:efl:canvas:event_grabber:method:group_member_add|Efl.Canvas.Event_Grabber.group_member_add]]
  * [[:develop:api:efl:ui:widget:method:group_member_add|Efl.Ui.Widget.group_member_add]]
  * [[:develop:api:efl:ui:image:method:group_member_add|Efl.Ui.Image.group_member_add]]
  * [[:develop:api:efl:ui:image_zoomable:method:group_member_add|Efl.Ui.Image_Zoomable.group_member_add]]
  * [[:develop:api:efl:ui:text:method:group_member_add|Efl.Ui.Text.group_member_add]]
  * [[:develop:api:efl:ui:panel:method:group_member_add|Efl.Ui.Panel.group_member_add]]

