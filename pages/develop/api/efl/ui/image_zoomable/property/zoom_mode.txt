~~Title: Efl.Ui.Image_Zoomable.zoom_mode~~
====== Efl.Ui.Image_Zoomable.zoom_mode ======

===== Description =====

%%Zoom mode.%%

%%This sets the zoom mode to manual or one of several automatic levels. %%[[:develop:api:efl:ui:zoom_mode|Efl.Ui.Zoom_Mode.manual]]%% means that zoom is controlled manually by %%[[:develop:api:efl:ui:zoom:property:zoom_level|Efl.Ui.Zoom.zoom_level]]%% and will stay at that level until changed by code or until %%[[:develop:api:efl:ui:zoom:property:zoom_mode|Efl.Ui.Zoom.zoom_mode]]%% is changed. This is the default mode. The Automatic modes will allow the zoomable object to automatically adjust zoom mode based on image and viewport size changes.%%
{{page>:develop:api-include:efl:ui:image_zoomable:property:zoom_mode:description&nouser&nolink&nodate}}

===== Values =====

  * **mode** - %%The zoom mode.%%

//Overridden from [[:develop:api:efl:ui:zoom:property:zoom_mode|Efl.Ui.Zoom.zoom_mode]] **(get, set)**.//===== Signature =====

<code>
@property zoom_mode @pure_virtual {
    get {}
    set {}
    values {
        mode: Efl.Ui.Zoom_Mode (Efl.Ui.Zoom_Mode.manual);
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Zoom_Mode efl_ui_zoom_mode_get(const Eo *obj);
void efl_ui_zoom_mode_set(Eo *obj, Efl_Ui_Zoom_Mode mode);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:zoom:property:zoom_mode|Efl.Ui.Zoom.zoom_mode]]
  * [[:develop:api:efl:ui:image_zoomable:property:zoom_mode|Efl.Ui.Image_Zoomable.zoom_mode]]

