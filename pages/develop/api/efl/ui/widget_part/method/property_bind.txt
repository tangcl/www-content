~~Title: Efl.Ui.Widget_Part.property_bind~~
====== Efl.Ui.Widget_Part.property_bind ======

===== Description =====

%%bind property data with the given key string. when the data is ready or changed, bind the data to the key action and process promised work.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:widget_part:method:property_bind:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:property_bind:method:property_bind|Efl.Ui.Property_Bind.property_bind]].//===== Signature =====

<code>
property_bind @pure_virtual {
    params {
        @in key: string;
        @in property: string;
    }
    return: Eina.Error;
}
</code>

===== C signature =====

<code c>
Eina_Error efl_ui_property_bind(Eo *obj, const char *key, const char *property);
</code>

===== Parameters =====

  * **key** //(in)// - %%key string for bind model property data%%
  * **property** //(in)// - %%Model property name%%

===== Implemented by =====

  * [[:develop:api:efl:ui:property_bind:method:property_bind|Efl.Ui.Property_Bind.property_bind]]
  * [[:develop:api:efl:ui:widget_factory:method:property_bind|Efl.Ui.Widget_Factory.property_bind]]
  * [[:develop:api:efl:ui:caching_factory:method:property_bind|Efl.Ui.Caching_Factory.property_bind]]
  * [[:develop:api:efl:ui:image_factory:method:property_bind|Efl.Ui.Image_Factory.property_bind]]
  * [[:develop:api:efl:ui:layout_factory:method:property_bind|Efl.Ui.Layout_Factory.property_bind]]
  * [[:develop:api:efl:ui:widget_part:method:property_bind|Efl.Ui.Widget_Part.property_bind]]
  * [[:develop:api:efl:ui:widget:method:property_bind|Efl.Ui.Widget.property_bind]]
  * [[:develop:api:efl:ui:image:method:property_bind|Efl.Ui.Image.property_bind]]
  * [[:develop:api:efl:ui:layout_base:method:property_bind|Efl.Ui.Layout_Base.property_bind]]
  * [[:develop:api:efl:ui:property_bind_part:method:property_bind|Efl.Ui.Property_Bind_Part.property_bind]]

