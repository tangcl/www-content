~~Title: Efl.Ui.Layout_Part_Box.orientation~~
====== Efl.Ui.Layout_Part_Box.orientation ======

===== Description =====

%%Control the direction of a given widget.%%

%%Use this function to change how your widget is to be disposed: vertically or horizontally or inverted vertically or inverted horizontally.%%

%%Mirroring as defined in %%[[:develop:api:efl:ui:i18n|Efl.Ui.I18n]]%% can invert the %%''horizontal''%% direction: it is %%''ltr''%% by default, but becomes %%''rtl''%% if the object is mirrored.%%

//Since 1.23//


{{page>:develop:api-include:efl:ui:layout_part_box:property:orientation:description&nouser&nolink&nodate}}

===== Values =====

  * **dir** - %%Direction of the widget.%%
==== Setter ====

%%This will always print an error saying that this is a read-only object.%%
{{page>:develop:api-include:efl:ui:layout_part_box:property:orientation:getter_description&nouser&nolink&nodate}}


//Overridden from [[:develop:api:efl:ui:layout_orientable_readonly:property:orientation|Efl.Ui.Layout_Orientable.orientation]] **(get)**.//===== Signature =====

<code>
@property orientation @pure_virtual {
    get {}
    set {}
    values {
        dir: Efl.Ui.Layout_Orientation;
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Layout_Orientation efl_ui_layout_orientation_get(const Eo *obj);
void efl_ui_layout_orientation_set(Eo *obj, Efl_Ui_Layout_Orientation dir);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:layout_orientable:property:orientation|Efl.Ui.Layout_Orientable.orientation]]
  * [[:develop:api:efl:ui:slider:property:orientation|Efl.Ui.Slider.orientation]]
  * [[:develop:api:efl:ui:layout_orientable_readonly:property:orientation|Efl.Ui.Layout_Orientable_Readonly.orientation]]
  * [[:develop:api:efl:canvas:layout_part_box:property:orientation|Efl.Canvas.Layout_Part_Box.orientation]]
  * [[:develop:api:efl:ui:layout_part_box:property:orientation|Efl.Ui.Layout_Part_Box.orientation]]
  * [[:develop:api:efl:canvas:layout_part_invalid:property:orientation|Efl.Canvas.Layout_Part_Invalid.orientation]]
  * [[:develop:api:efl:ui:collection_view:property:orientation|Efl.Ui.Collection_View.orientation]]
  * [[:develop:api:efl:ui:table:property:orientation|Efl.Ui.Table.orientation]]
  * [[:develop:api:efl:ui:box:property:orientation|Efl.Ui.Box.orientation]]
  * [[:develop:api:efl:ui:slider_interval:property:orientation|Efl.Ui.Slider_Interval.orientation]]
  * [[:develop:api:efl:ui:collection:property:orientation|Efl.Ui.Collection.orientation]]
  * [[:develop:api:efl:ui:panes:property:orientation|Efl.Ui.Panes.orientation]]
  * [[:develop:api:efl:ui:progressbar:property:orientation|Efl.Ui.Progressbar.orientation]]
  * [[:develop:api:efl:ui:spin_button:property:orientation|Efl.Ui.Spin_Button.orientation]]
  * [[:develop:api:efl:ui:position_manager:grid:property:orientation|Efl.Ui.Position_Manager.Grid.orientation]]
  * [[:develop:api:efl:ui:position_manager:list:property:orientation|Efl.Ui.Position_Manager.List.orientation]]

