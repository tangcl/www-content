~~Title: Efl.Ui.Layout_Part_Box.unpack_all~~
====== Efl.Ui.Layout_Part_Box.unpack_all ======

===== Description =====

%%Removes all packed sub-objects without unreferencing them.%%

%%Use with caution.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:layout_part_box:method:unpack_all:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack:method:unpack_all|Efl.Pack.unpack_all]].//===== Signature =====

<code>
unpack_all @pure_virtual {
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_unpack_all(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:pack:method:unpack_all|Efl.Pack.unpack_all]]
  * [[:develop:api:efl:ui:tab_bar:method:unpack_all|Efl.Ui.Tab_Bar.unpack_all]]
  * [[:develop:api:efl:ui:table:method:unpack_all|Efl.Ui.Table.unpack_all]]
  * [[:develop:api:efl:canvas:layout_part_table:method:unpack_all|Efl.Canvas.Layout_Part_Table.unpack_all]]
  * [[:develop:api:efl:ui:layout_part_table:method:unpack_all|Efl.Ui.Layout_Part_Table.unpack_all]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:unpack_all|Efl.Canvas.Layout_Part_Invalid.unpack_all]]
  * [[:develop:api:efl:canvas:layout_part_box:method:unpack_all|Efl.Canvas.Layout_Part_Box.unpack_all]]
  * [[:develop:api:efl:ui:box:method:unpack_all|Efl.Ui.Box.unpack_all]]
  * [[:develop:api:efl:ui:radio_box:method:unpack_all|Efl.Ui.Radio_Box.unpack_all]]
  * [[:develop:api:efl:ui:group_item:method:unpack_all|Efl.Ui.Group_Item.unpack_all]]
  * [[:develop:api:efl:ui:collection:method:unpack_all|Efl.Ui.Collection.unpack_all]]
  * [[:develop:api:efl:ui:layout_part_box:method:unpack_all|Efl.Ui.Layout_Part_Box.unpack_all]]
  * [[:develop:api:efl:ui:spotlight:container:method:unpack_all|Efl.Ui.Spotlight.Container.unpack_all]]
  * [[:develop:api:efl:ui:tab_pager:method:unpack_all|Efl.Ui.Tab_Pager.unpack_all]]
  * [[:develop:api:efl:ui:relative_layout:method:unpack_all|Efl.Ui.Relative_Layout.unpack_all]]

