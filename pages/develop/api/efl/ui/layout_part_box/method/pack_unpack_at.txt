~~Title: Efl.Ui.Layout_Part_Box.pack_unpack_at~~
====== Efl.Ui.Layout_Part_Box.pack_unpack_at ======

===== Description =====

%%Pop out (remove) the sub-object at the specified %%''index''%%.%%

%%%%''index''%% ranges from %%''-count''%% to %%''count-1''%%, where positive numbers go from first sub-object (%%''0''%%) to last (%%''count-1''%%), and negative numbers go from last sub-object (%%''-1''%%) to first (%%''-count''%%). %%''count''%% is the number of sub-objects currently in the container as returned by %%[[:develop:api:efl:container:method:content_count|Efl.Container.content_count]]%%.%%

%%If %%''index''%% is less than -%%''count''%%, it will remove the first sub-object whereas %%''index''%% greater than %%''count''%%-1 will remove the last sub-object.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:layout_part_box:method:pack_unpack_at:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_linear:method:pack_unpack_at|Efl.Pack_Linear.pack_unpack_at]].//===== Signature =====

<code>
pack_unpack_at @pure_virtual {
    params {
        @in index: int;
    }
    return: Efl.Gfx.Entity;
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Entity *efl_pack_unpack_at(Eo *obj, int index);
</code>

===== Parameters =====

  * **index** //(in)// - %%Index of the sub-object to remove. Valid range is %%''-count''%% to %%''count-1''%%.%%

===== Implemented by =====

  * [[:develop:api:efl:pack_linear:method:pack_unpack_at|Efl.Pack_Linear.pack_unpack_at]]
  * [[:develop:api:efl:ui:tab_bar:method:pack_unpack_at|Efl.Ui.Tab_Bar.pack_unpack_at]]
  * [[:develop:api:efl:ui:flip:method:pack_unpack_at|Efl.Ui.Flip.pack_unpack_at]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_unpack_at|Efl.Canvas.Layout_Part_Box.pack_unpack_at]]
  * [[:develop:api:efl:ui:box:method:pack_unpack_at|Efl.Ui.Box.pack_unpack_at]]
  * [[:develop:api:efl:ui:radio_box:method:pack_unpack_at|Efl.Ui.Radio_Box.pack_unpack_at]]
  * [[:develop:api:efl:ui:group_item:method:pack_unpack_at|Efl.Ui.Group_Item.pack_unpack_at]]
  * [[:develop:api:efl:ui:collection:method:pack_unpack_at|Efl.Ui.Collection.pack_unpack_at]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_unpack_at|Efl.Ui.Layout_Part_Box.pack_unpack_at]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_unpack_at|Efl.Canvas.Layout_Part_Invalid.pack_unpack_at]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_unpack_at|Efl.Ui.Spotlight.Container.pack_unpack_at]]
  * [[:develop:api:efl:ui:tab_pager:method:pack_unpack_at|Efl.Ui.Tab_Pager.pack_unpack_at]]

