~~Title: Efl.Ui.Collection_View.invalidate~~
====== Efl.Ui.Collection_View.invalidate ======

===== Description =====

%%Implement this method to perform special actions when your object loses its parent, if you need to.%%

%%It is called when the parent reference is lost or set to %%''NULL''%%. After this call returns, %%[[:develop:api:efl:object:property:invalidated|Efl.Object.invalidated]]%% is set to %%''true''%%. This allows a simpler tear down of complex hierarchies, by performing object destruction in two steps, first all object relationships are broken and then the isolated objects are destroyed. Performing everything in the %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%% can sometimes lead to deadlocks, but implementing this method is optional if this is not your case. When an object with a parent is destroyed, it first receives a call to %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%% and then to %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%. See the Life Cycle section in this class' description.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:collection_view:method:invalidate:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:layout_base:method:invalidate|Efl.Object.invalidate]].//===== Signature =====

<code>
invalidate {}
</code>

===== C signature =====

<code c>
void efl_invalidate(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]
  * [[:develop:api:efl:canvas:vg:node:method:invalidate|Efl.Canvas.Vg.Node.invalidate]]
  * [[:develop:api:efl:ui:position_manager:grid:method:invalidate|Efl.Ui.Position_Manager.Grid.invalidate]]
  * [[:develop:api:efl:ui:spotlight:manager_scroll:method:invalidate|Efl.Ui.Spotlight.Manager_Scroll.invalidate]]
  * [[:develop:api:efl:ui:spotlight:manager_stack:method:invalidate|Efl.Ui.Spotlight.Manager_Stack.invalidate]]
  * [[:develop:api:efl:net:server_udp_client:method:invalidate|Efl.Net.Server_Udp_Client.invalidate]]
  * [[:develop:api:efl:net:server_windows:method:invalidate|Efl.Net.Server_Windows.invalidate]]
  * [[:develop:api:efl:io:buffered_stream:method:invalidate|Efl.Io.Buffered_Stream.invalidate]]
  * [[:develop:api:efl:net:dialer_simple:method:invalidate|Efl.Net.Dialer_Simple.invalidate]]
  * [[:develop:api:efl:loop_fd:method:invalidate|Efl.Loop_Fd.invalidate]]
  * [[:develop:api:efl:net:server_ssl:method:invalidate|Efl.Net.Server_Ssl.invalidate]]
  * [[:develop:api:efl:net:socket_fd:method:invalidate|Efl.Net.Socket_Fd.invalidate]]
  * [[:develop:api:efl:net:dialer_udp:method:invalidate|Efl.Net.Dialer_Udp.invalidate]]
  * [[:develop:api:efl:net:dialer_unix:method:invalidate|Efl.Net.Dialer_Unix.invalidate]]
  * [[:develop:api:efl:net:dialer_tcp:method:invalidate|Efl.Net.Dialer_Tcp.invalidate]]
  * [[:develop:api:efl:net:socket_ssl:method:invalidate|Efl.Net.Socket_Ssl.invalidate]]
  * [[:develop:api:efl:net:dialer_ssl:method:invalidate|Efl.Net.Dialer_Ssl.invalidate]]
  * [[:develop:api:efl:canvas:object:method:invalidate|Efl.Canvas.Object.invalidate]]
  * [[:develop:api:efl:canvas:layout:method:invalidate|Efl.Canvas.Layout.invalidate]]
  * [[:develop:api:efl:ui:widget:method:invalidate|Efl.Ui.Widget.invalidate]]
  * [[:develop:api:efl:ui:table:method:invalidate|Efl.Ui.Table.invalidate]]
  * [[:develop:api:efl:ui:box:method:invalidate|Efl.Ui.Box.invalidate]]
  * [[:develop:api:efl:ui:image:method:invalidate|Efl.Ui.Image.invalidate]]
  * [[:develop:api:efl:ui:layout_base:method:invalidate|Efl.Ui.Layout_Base.invalidate]]
  * [[:develop:api:efl:ui:collection_view:method:invalidate|Efl.Ui.Collection_View.invalidate]]
  * [[:develop:api:efl:ui:group_item:method:invalidate|Efl.Ui.Group_Item.invalidate]]
  * [[:develop:api:efl:ui:collection:method:invalidate|Efl.Ui.Collection.invalidate]]
  * [[:develop:api:efl:ui:spotlight:container:method:invalidate|Efl.Ui.Spotlight.Container.invalidate]]
  * [[:develop:api:efl:ui:relative_layout:method:invalidate|Efl.Ui.Relative_Layout.invalidate]]
  * [[:develop:api:efl:canvas:vg:object:method:invalidate|Efl.Canvas.Vg.Object.invalidate]]
  * [[:develop:api:efl:loop_model:method:invalidate|Efl.Loop_Model.invalidate]]
  * [[:develop:api:eldbus:model:method:invalidate|Eldbus.Model.invalidate]]
  * [[:develop:api:eldbus:model:connection:method:invalidate|Eldbus.Model.Connection.invalidate]]
  * [[:develop:api:eldbus:model:proxy:method:invalidate|Eldbus.Model.Proxy.invalidate]]
  * [[:develop:api:eldbus:model:arguments:method:invalidate|Eldbus.Model.Arguments.invalidate]]
  * [[:develop:api:eldbus:model:signal:method:invalidate|Eldbus.Model.Signal.invalidate]]
  * [[:develop:api:eldbus:model:object:method:invalidate|Eldbus.Model.Object.invalidate]]
  * [[:develop:api:efl:composite_model:method:invalidate|Efl.Composite_Model.invalidate]]
  * [[:develop:api:efl:ui:select_model:method:invalidate|Efl.Ui.Select_Model.invalidate]]
  * [[:develop:api:efl:io:model:method:invalidate|Efl.Io.Model.invalidate]]
  * [[:develop:api:efl:net:dialer_websocket:method:invalidate|Efl.Net.Dialer_Websocket.invalidate]]
  * [[:develop:api:efl:net:server_simple:method:invalidate|Efl.Net.Server_Simple.invalidate]]
  * [[:develop:api:efl:ui:caching_factory:method:invalidate|Efl.Ui.Caching_Factory.invalidate]]
  * [[:develop:api:efl:loop:method:invalidate|Efl.Loop.invalidate]]
  * [[:develop:api:efl:net:dialer_http:method:invalidate|Efl.Net.Dialer_Http.invalidate]]
  * [[:develop:api:efl:io:copier:method:invalidate|Efl.Io.Copier.invalidate]]
  * [[:develop:api:efl:ui:position_manager:list:method:invalidate|Efl.Ui.Position_Manager.List.invalidate]]

