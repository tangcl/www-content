~~Title: Efl.Object.name~~
====== Efl.Object.name ======

===== Description =====

%%The name of the object.%%

%%Every EFL object can have a name. Names may not contain the following characters: / ? * [ ] !  : Using any of these in a name will result in undefined behavior later on. An empty string is considered the same as a %%''NULL''%% string or no string for the name.%%

//Since 1.22//
{{page>:develop:api-include:efl:object:property:name:description&nouser&nolink&nodate}}

===== Values =====

  * **name** - %%The name.%%

===== Signature =====

<code>
@property name {
    get {}
    set {}
    values {
        name: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_name_get(const Eo *obj);
void efl_name_set(Eo *obj, const char *name);
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:property:name|Efl.Object.name]]
  * [[:develop:api:efl:canvas:vg:node:property:name|Efl.Canvas.Vg.Node.name]]
  * [[:develop:api:efl:net:control:technology:property:name|Efl.Net.Control.Technology.name]]

