~~Title: Efl.Object.event_global_thaw~~
====== Efl.Object.event_global_thaw ======

===== Description =====

%%Globally thaw events for ALL EFL OBJECTS.%%

%%Allows event callbacks to be called for all EFL objects after they have been disabled by %%[[:develop:api:efl:object:method:event_global_freeze|Efl.Object.event_global_freeze]]%%. The amount of thaws must match the amount of freezes for events to be re-enabled.%%

//Since 1.22//
{{page>:develop:api-include:efl:object:method:event_global_thaw:description&nouser&nolink&nodate}}

===== Signature =====

<code>
event_global_thaw @static {}
</code>

===== C signature =====

<code c>
void efl_event_global_thaw();
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:method:event_global_thaw|Efl.Object.event_global_thaw]]

