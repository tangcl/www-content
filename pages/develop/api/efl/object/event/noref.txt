~~Title: Efl.Object: noref~~

===== Description =====

%%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%%

//Since 1.22//

{{page>:develop:api-include:efl:object:event:noref:description&nouser&nolink&nodate}}

===== Signature =====

<code>
noref @hot;
</code>

===== C information =====

<code c>
EFL_EVENT_NOREF(void, @hot)
</code>

===== C usage =====

<code c>
static void
on_efl_event_noref(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_EVENT_NOREF, on_efl_event_noref, d);
}

</code>
