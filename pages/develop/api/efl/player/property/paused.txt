~~Title: Efl.Player.paused~~
====== Efl.Player.paused ======

===== Description =====

%%Pause state of the media file.%%

%%This property sets the pause state of the media.  Re-setting the current pause state has no effect.%%

%%If %%[[:develop:api:efl:player:property:playing|Efl.Player.playing]]%% is set to %%''true''%%, this property can be used to pause and resume playback of the media without changing its %%[[:develop:api:efl:player:property:playback_progress|Efl.Player.playback_progress]]%% property. This property cannot be changed if %%[[:develop:api:efl:player:property:playing|Efl.Player.playing]]%% is %%''false''%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:player:property:paused:description&nouser&nolink&nodate}}

===== Values =====

  * **paused** - %%%%''true''%% if paused, %%''false''%% otherwise.%%

===== Signature =====

<code>
@property paused @pure_virtual {
    get {}
    set {
        return: bool (false);
    }
    values {
        paused: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_player_paused_get(const Eo *obj);
Eina_Bool efl_player_paused_set(Eo *obj, Eina_Bool paused);
</code>

===== Implemented by =====

  * [[:develop:api:efl:player:property:paused|Efl.Player.paused]]
  * [[:develop:api:efl:canvas:video:property:paused|Efl.Canvas.Video.paused]]
  * [[:develop:api:efl:canvas:animation_player:property:paused|Efl.Canvas.Animation_Player.paused]]
  * [[:develop:api:efl:canvas:layout:property:paused|Efl.Canvas.Layout.paused]]
  * [[:develop:api:efl:ui:image:property:paused|Efl.Ui.Image.paused]]
  * [[:develop:api:efl:ui:image_zoomable:property:paused|Efl.Ui.Image_Zoomable.paused]]
  * [[:develop:api:efl:ui:video:property:paused|Efl.Ui.Video.paused]]

