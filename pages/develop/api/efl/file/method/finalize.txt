~~Title: Efl.File.finalize~~
====== Efl.File.finalize ======

===== Description =====

No description supplied.
{{page>:develop:api-include:efl:file:method:finalize:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:file:method:finalize|Efl.Object.finalize]].//===== Signature =====

<code>
finalize {
    return: Efl.Object;
}
</code>

===== C signature =====

<code c>
Efl_Object *efl_finalize(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:method:finalize|Efl.Object.finalize]]
  * [[:develop:api:efl:loop_handler:method:finalize|Efl.Loop_Handler.finalize]]
  * [[:develop:api:efl:ui:position_manager:grid:method:finalize|Efl.Ui.Position_Manager.Grid.finalize]]
  * [[:develop:api:efl:canvas:layout_part:method:finalize|Efl.Canvas.Layout_Part.finalize]]
  * [[:develop:api:efl:net:server_udp_client:method:finalize|Efl.Net.Server_Udp_Client.finalize]]
  * [[:develop:api:efl:ui:widget_part_bg:method:finalize|Efl.Ui.Widget_Part_Bg.finalize]]
  * [[:develop:api:efl:ui:layout_part_bg:method:finalize|Efl.Ui.Layout_Part_Bg.finalize]]
  * [[:develop:api:efl:io:buffered_stream:method:finalize|Efl.Io.Buffered_Stream.finalize]]
  * [[:develop:api:efl:net:dialer_simple:method:finalize|Efl.Net.Dialer_Simple.finalize]]
  * [[:develop:api:efl:io:file:method:finalize|Efl.Io.File.finalize]]
  * [[:develop:api:efl:net:server_fd:method:finalize|Efl.Net.Server_Fd.finalize]]
  * [[:develop:api:efl:io:stderr:method:finalize|Efl.Io.Stderr.finalize]]
  * [[:develop:api:efl:io:stdin:method:finalize|Efl.Io.Stdin.finalize]]
  * [[:develop:api:efl:io:stdout:method:finalize|Efl.Io.Stdout.finalize]]
  * [[:develop:api:efl:net:socket_fd:method:finalize|Efl.Net.Socket_Fd.finalize]]
  * [[:develop:api:efl:net:socket_ssl:method:finalize|Efl.Net.Socket_Ssl.finalize]]
  * [[:develop:api:efl:net:dialer_ssl:method:finalize|Efl.Net.Dialer_Ssl.finalize]]
  * [[:develop:api:efl:canvas:object:method:finalize|Efl.Canvas.Object.finalize]]
  * [[:develop:api:efl:canvas:image_internal:method:finalize|Efl.Canvas.Image_Internal.finalize]]
  * [[:develop:api:efl:ui:internal:text:interactive:method:finalize|Efl.Ui.Internal.Text.Interactive.finalize]]
  * [[:develop:api:efl:ui:widget:method:finalize|Efl.Ui.Widget.finalize]]
  * [[:develop:api:efl:ui:win:method:finalize|Efl.Ui.Win.finalize]]
  * [[:develop:api:efl:ui:win_socket:method:finalize|Efl.Ui.Win_Socket.finalize]]
  * [[:develop:api:efl:ui:win_inlined:method:finalize|Efl.Ui.Win_Inlined.finalize]]
  * [[:develop:api:efl:ui:layout_base:method:finalize|Efl.Ui.Layout_Base.finalize]]
  * [[:develop:api:efl:ui:text:method:finalize|Efl.Ui.Text.finalize]]
  * [[:develop:api:efl:ui:tab_bar:method:finalize|Efl.Ui.Tab_Bar.finalize]]
  * [[:develop:api:efl:ui:item:method:finalize|Efl.Ui.Item.finalize]]
  * [[:develop:api:efl:ui:list_placeholder_item:method:finalize|Efl.Ui.List_Placeholder_Item.finalize]]
  * [[:develop:api:elm:code_widget:method:finalize|Elm.Code_Widget.finalize]]
  * [[:develop:api:efl:ui:scroller:method:finalize|Efl.Ui.Scroller.finalize]]
  * [[:develop:api:efl:ui:internal_text_scroller:method:finalize|Efl.Ui.Internal_Text_Scroller.finalize]]
  * [[:develop:api:efl:ui:collection:method:finalize|Efl.Ui.Collection.finalize]]
  * [[:develop:api:efl:ui:spotlight:container:method:finalize|Efl.Ui.Spotlight.Container.finalize]]
  * [[:develop:api:efl:ui:tab_pager:method:finalize|Efl.Ui.Tab_Pager.finalize]]
  * [[:develop:api:efl:ui:bg:method:finalize|Efl.Ui.Bg.finalize]]
  * [[:develop:api:efl:canvas:vg:object:method:finalize|Efl.Canvas.Vg.Object.finalize]]
  * [[:develop:api:efl:loop_timer:method:finalize|Efl.Loop_Timer.finalize]]
  * [[:develop:api:eldbus:model:method:finalize|Eldbus.Model.finalize]]
  * [[:develop:api:eldbus:model:proxy:method:finalize|Eldbus.Model.Proxy.finalize]]
  * [[:develop:api:eldbus:model:arguments:method:finalize|Eldbus.Model.Arguments.finalize]]
  * [[:develop:api:eldbus:model:method:method:finalize|Eldbus.Model.Method.finalize]]
  * [[:develop:api:eldbus:model:object:method:finalize|Eldbus.Model.Object.finalize]]
  * [[:develop:api:efl:composite_model:method:finalize|Efl.Composite_Model.finalize]]
  * [[:develop:api:efl:container_model:method:finalize|Efl.Container_Model.finalize]]
  * [[:develop:api:efl:ui:view_model:method:finalize|Efl.Ui.View_Model.finalize]]
  * [[:develop:api:efl:io:model:method:finalize|Efl.Io.Model.finalize]]
  * [[:develop:api:efl:net:server_simple:method:finalize|Efl.Net.Server_Simple.finalize]]
  * [[:develop:api:efl:net:control:manager:method:finalize|Efl.Net.Control.Manager.finalize]]
  * [[:develop:api:efl:ui:widget_factory:method:finalize|Efl.Ui.Widget_Factory.finalize]]
  * [[:develop:api:efl:ui:caching_factory:method:finalize|Efl.Ui.Caching_Factory.finalize]]
  * [[:develop:api:efl:thread:method:finalize|Efl.Thread.finalize]]
  * [[:develop:api:efl:net:session:method:finalize|Efl.Net.Session.finalize]]
  * [[:develop:api:efl:io:copier:method:finalize|Efl.Io.Copier.finalize]]
  * [[:develop:api:efl:ui:focus:manager_calc:method:finalize|Efl.Ui.Focus.Manager_Calc.finalize]]
  * [[:develop:api:efl:ui:focus:manager_root_focus:method:finalize|Efl.Ui.Focus.Manager_Root_Focus.finalize]]
  * [[:develop:api:evas:canvas3d:node:method:finalize|Evas.Canvas3D.Node.finalize]]
  * [[:develop:api:efl:net:ssl:context:method:finalize|Efl.Net.Ssl.Context.finalize]]
  * [[:develop:api:efl:io:buffer:method:finalize|Efl.Io.Buffer.finalize]]
  * [[:develop:api:efl:net:ip_address:method:finalize|Efl.Net.Ip_Address.finalize]]
  * [[:develop:api:efl:io:queue:method:finalize|Efl.Io.Queue.finalize]]
  * [[:develop:api:ector:renderer:method:finalize|Ector.Renderer.finalize]]

