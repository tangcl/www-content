~~Title: Efl.Model.property~~
====== Efl.Model.property ======

===== Description =====

%%No description supplied.%%

//Since 1.23//


{{page>:develop:api-include:efl:model:property:property:description&nouser&nolink&nodate}}

===== Keys =====

  * **property** - %%Property name%%
===== Values =====

  * **value** - %%Property value%%
==== Getter ====

%%Retrieve the value of a given property name.%%

%%At this point the caller is free to get values from properties. The event %%[[:develop:api:efl:model:event:properties,changed|Efl.Model.properties,changed]]%% may be raised to notify listeners of the property/value.%%

%%See %%[[:develop:api:efl:model:property:properties|Efl.Model.properties.get]]%%, %%[[:develop:api:efl:model:event:properties,changed|Efl.Model.properties,changed]]%%%%

//Since 1.23//


{{page>:develop:api-include:efl:model:property:property:getter_description&nouser&nolink&nodate}}

==== Setter ====

%%Set a property value of a given property name.%%

%%The caller must first read %%[[:develop:api:efl:model:property:properties|Efl.Model.properties]]%% to obtain the list of available properties before being able to access them through %%[[:develop:api:efl:model:property:property|Efl.Model.property]]%%. This function sets a new property value into given property name. Once the operation is completed the concrete implementation should raise %%[[:develop:api:efl:model:event:properties,changed|Efl.Model.properties,changed]]%% event in order to notify listeners of the new value of the property.%%

%%If the model doesn't have the property then there are two possibilities, either raise an error or create the new property in model%%

%%See %%[[:develop:api:efl:model:property:property|Efl.Model.property.get]]%%, %%[[:develop:api:efl:model:event:properties,changed|Efl.Model.properties,changed]]%%%%

//Since 1.23//
{{page>:develop:api-include:efl:model:property:property:getter_description&nouser&nolink&nodate}}


===== Signature =====

<code>
@property property @pure_virtual {
    get {}
    set {
        return: future<any_value_ref>;
    }
    keys {
        property: string;
    }
    values {
        value: any_value_ref;
    }
}
</code>

===== C signature =====

<code c>
Eina_Value *efl_model_property_get(const Eo *obj, const char *property);
Eina_Future *efl_model_property_set(Eo *obj, const char *property, Eina_Value *value);
</code>

===== Implemented by =====

  * [[:develop:api:efl:model:property:property|Efl.Model.property]]
  * [[:develop:api:efl:mono_model_internal_child:property:property|Efl.Mono_Model_Internal_Child.property]]
  * [[:develop:api:efl:mono_model_internal:property:property|Efl.Mono_Model_Internal.property]]
  * [[:develop:api:efl:loop_model:property:property|Efl.Loop_Model.property]]
  * [[:develop:api:eldbus:model:property:property|Eldbus.Model.property]]
  * [[:develop:api:eldbus:model:proxy:property:property|Eldbus.Model.Proxy.property]]
  * [[:develop:api:eldbus:model:arguments:property:property|Eldbus.Model.Arguments.property]]
  * [[:develop:api:efl:composite_model:property:property|Efl.Composite_Model.property]]
  * [[:develop:api:efl:boolean_model:property:property|Efl.Boolean_Model.property]]
  * [[:develop:api:efl:ui:select_model:property:property|Efl.Ui.Select_Model.property]]
  * [[:develop:api:efl:ui:state_model:property:property|Efl.Ui.State_Model.property]]
  * [[:develop:api:efl:container_model:property:property|Efl.Container_Model.property]]
  * [[:develop:api:efl:ui:view_model:property:property|Efl.Ui.View_Model.property]]
  * [[:develop:api:efl:ui:homogeneous_model:property:property|Efl.Ui.Homogeneous_Model.property]]
  * [[:develop:api:efl:ui:exact_model:property:property|Efl.Ui.Exact_Model.property]]
  * [[:develop:api:efl:ui:average_model:property:property|Efl.Ui.Average_Model.property]]
  * [[:develop:api:efl:filter_model:property:property|Efl.Filter_Model.property]]
  * [[:develop:api:efl:io:model:property:property|Efl.Io.Model.property]]
  * [[:develop:api:efl:generic_model:property:property|Efl.Generic_Model.property]]

