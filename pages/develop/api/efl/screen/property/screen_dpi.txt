~~Title: Efl.Screen.screen_dpi~~
====== Efl.Screen.screen_dpi ======

===== Values =====

  * **xdpi** - %%Horizontal DPI.%%
  * **ydpi** - %%Vertical DPI.%%


\\ {{page>:develop:api-include:efl:screen:property:screen_dpi:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property screen_dpi @pure_virtual {
    get {}
    values {
        xdpi: int;
        ydpi: int;
    }
}
</code>

===== C signature =====

<code c>
void efl_screen_dpi_get(const Eo *obj, int *xdpi, int *ydpi);
</code>

===== Implemented by =====

  * [[:develop:api:efl:screen:property:screen_dpi|Efl.Screen.screen_dpi]]
  * [[:develop:api:efl:ui:win:property:screen_dpi|Efl.Ui.Win.screen_dpi]]

