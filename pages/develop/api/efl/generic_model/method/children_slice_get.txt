~~Title: Efl.Generic_Model.children_slice_get~~
====== Efl.Generic_Model.children_slice_get ======

===== Description =====

%%Get children slice OR full range.%%

%%%%[[:develop:api:efl:model:method:children_slice_get|Efl.Model.children_slice_get]]%% behaves in two different ways, it may provide the slice if %%''count''%% is non-zero OR full range otherwise.%%

%%Since 'slice' is a range, for example if we have 20 children a slice could be the range from 3(start) with 4(count), see:%%

%%child 0  [no] child 1  [no] child 2  [no] child 3  [yes] child 4  [yes] child 5  [yes] child 6  [yes] child 7  [no]%%

%%Optionally the user can call %%[[:develop:api:efl:model:property:children_count|Efl.Model.children_count.get]]%% to know the number of children so a valid range can be known in advance.%%

%%See %%[[:develop:api:efl:model:property:children_count|Efl.Model.children_count.get]]%%%%

<note>
%%The returned children will live only as long as the future itself. Once the future is done, if you want to keep the object alive, you need to take a reference for yourself.%%
</note>

//Since 1.23//
{{page>:develop:api-include:efl:generic_model:method:children_slice_get:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:model:method:children_slice_get|Efl.Model.children_slice_get]].//===== Signature =====

<code>
children_slice_get @pure_virtual {
    params {
        @in start: uint;
        @in count: uint;
    }
    return: future<array<Efl.Object>>;
}
</code>

===== C signature =====

<code c>
Eina_Future *efl_model_children_slice_get(Eo *obj, unsigned int start, unsigned int count);
</code>

===== Parameters =====

  * **start** //(in)// - %%Range begin - start from here.%%
  * **count** //(in)// - %%Range size. If count is 0, start is ignored.%%

===== Implemented by =====

  * [[:develop:api:efl:model:method:children_slice_get|Efl.Model.children_slice_get]]
  * [[:develop:api:efl:mono_model_internal:method:children_slice_get|Efl.Mono_Model_Internal.children_slice_get]]
  * [[:develop:api:eldbus:model:method:children_slice_get|Eldbus.Model.children_slice_get]]
  * [[:develop:api:eldbus:model:connection:method:children_slice_get|Eldbus.Model.Connection.children_slice_get]]
  * [[:develop:api:eldbus:model:proxy:method:children_slice_get|Eldbus.Model.Proxy.children_slice_get]]
  * [[:develop:api:eldbus:model:object:method:children_slice_get|Eldbus.Model.Object.children_slice_get]]
  * [[:develop:api:efl:composite_model:method:children_slice_get|Efl.Composite_Model.children_slice_get]]
  * [[:develop:api:efl:ui:view_model:method:children_slice_get|Efl.Ui.View_Model.children_slice_get]]
  * [[:develop:api:efl:filter_model:method:children_slice_get|Efl.Filter_Model.children_slice_get]]
  * [[:develop:api:efl:io:model:method:children_slice_get|Efl.Io.Model.children_slice_get]]
  * [[:develop:api:efl:generic_model:method:children_slice_get|Efl.Generic_Model.children_slice_get]]

