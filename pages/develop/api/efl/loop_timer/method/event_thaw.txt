~~Title: Efl.Loop_Timer.event_thaw~~
====== Efl.Loop_Timer.event_thaw ======

===== Description =====

%%Thaw events of object.%%

%%Allows event callbacks to be called again for this object after a call to %%[[:develop:api:efl:object:method:event_freeze|Efl.Object.event_freeze]]%%. The amount of thaws must match the amount of freezes for events to be re-enabled.%%

//Since 1.22//
{{page>:develop:api-include:efl:loop_timer:method:event_thaw:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:object:method:event_thaw|Efl.Object.event_thaw]].//===== Signature =====

<code>
event_thaw {}
</code>

===== C signature =====

<code c>
void efl_event_thaw(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:method:event_thaw|Efl.Object.event_thaw]]
  * [[:develop:api:efl:canvas:object:method:event_thaw|Efl.Canvas.Object.event_thaw]]
  * [[:develop:api:efl:loop_timer:method:event_thaw|Efl.Loop_Timer.event_thaw]]

