~~Title: Efl.Loop_Timer~~
====== Efl.Loop_Timer (class) ======

===== Description =====

%%Timers are objects that will call a given callback at some point in the future and repeat that tick at a given interval.%%

%%Timers require the ecore main loop to be running and functioning properly. They do not guarantee exact timing but try to work on a "best effort" basis.%%

%%The %%[[:develop:api:efl:object:method:event_freeze|Efl.Object.event_freeze]]%% and %%[[:develop:api:efl:object:method:event_thaw|Efl.Object.event_thaw]]%% calls are used to pause and unpause the timer.%%

//Since 1.22//

{{page>:develop:api-include:efl:loop_timer:description&nouser&nolink&nodate}}

===== Inheritance =====

 => [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] //(class)// => [[:develop:api:efl:object|Efl.Object]] //(class)//
++++ Full hierarchy |

  * [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] //(class)//
    * [[:develop:api:efl:object|Efl.Object]] //(class)//


++++
===== Members =====

**[[:develop:api:efl:loop_timer:method:constructor|constructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to provide optional initialization code for your object.%%
<code c>
Efl_Object *efl_constructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_timer:method:destructor|destructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to provide deinitialization code for your object if you need it.%%
<code c>
void efl_destructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_timer:method:event_freeze|event_freeze]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Freeze events of this object.%%
<code c>
void efl_event_freeze(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_timer:property:event_freeze_count|event_freeze_count]]** //**(get)**//// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> 
<code c>
int efl_event_freeze_count_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_timer:method:event_thaw|event_thaw]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Thaw events of object.%%
<code c>
void efl_event_thaw(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_timer:method:finalize|finalize]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to finish the initialization of your object after all (if any) user-provided configuration methods have been executed.%%
<code c>
Efl_Object *efl_finalize(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_timer:property:parent|parent]]** //**(get, set)**//// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%The parent of an object.%%
<code c>
Efl_Object *efl_parent_get(const Eo *obj);
void efl_parent_set(Eo *obj, Efl_Object *parent);
</code>
\\
**[[:develop:api:efl:loop_timer:property:time_pending|time_pending]]** //**(get)**//\\
> 
<code c>
double efl_loop_timer_time_pending_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_timer:method:timer_delay|timer_delay]]**\\
> %%Adds a delay to the next occurrence of a timer. This doesn't affect the timer interval.%%
<code c>
void efl_loop_timer_delay(Eo *obj, double add);
</code>
\\
**[[:develop:api:efl:loop_timer:property:timer_interval|timer_interval]]** //**(get, set)**//\\
> %%Interval the timer ticks on.%%
<code c>
double efl_loop_timer_interval_get(const Eo *obj);
void efl_loop_timer_interval_set(Eo *obj, double in);
</code>
\\
**[[:develop:api:efl:loop_timer:method:timer_loop_reset|timer_loop_reset]]**\\
> %%This effectively resets a timer but based on the time when this iteration of the main loop started.%%
<code c>
void efl_loop_timer_loop_reset(Eo *obj);
</code>
\\
**[[:develop:api:efl:loop_timer:method:timer_reset|timer_reset]]**\\
> %%Resets a timer to its full interval. This effectively makes the timer start ticking off from zero now.%%
<code c>
void efl_loop_timer_reset(Eo *obj);
</code>
\\

==== Inherited ====

^ [[:develop:api:efl:loop_consumer|Efl.Loop_Consumer]] ^^^
|  | **[[:develop:api:efl:loop_consumer:method:future_rejected|future_rejected]]** | %%Creates a new future that is already rejected to a specified error using the %%[[:develop:api:efl:loop_consumer:property:loop|Efl.Loop_Consumer.loop.get]]%%.%% |
|  | **[[:develop:api:efl:loop_consumer:method:future_resolved|future_resolved]]** | %%Creates a new future that is already resolved to a value.%% |
|  | **[[:develop:api:efl:loop_consumer:property:loop|loop]]** //**(get)**// |  |
|  | **[[:develop:api:efl:loop_consumer:method:promise_new|promise_new]]** | %%Create a new promise with the scheduler coming from the loop provided by this object.%% |
^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:property:allow_parent_unref|allow_parent_unref]]** //**(get, set)**// | %%Allow an object to be deleted by unref even if it has a parent.%% |
|  | **[[:develop:api:efl:object:method:children_iterator_new|children_iterator_new]]** | %%Get an iterator on all children.%% |
|  | **[[:develop:api:efl:object:property:comment|comment]]** //**(get, set)**// | %%A human readable comment for the object.%% |
|  | **[[:develop:api:efl:object:method:composite_attach|composite_attach]]** | %%Make an object a composite object of another.%% |
|  | **[[:develop:api:efl:object:method:composite_detach|composite_detach]]** | %%Detach a composite object from another object.%% |
|  | **[[:develop:api:efl:object:method:composite_part_is|composite_part_is]]** | %%Check if an object is part of a composite object.%% |
|  | **[[:develop:api:efl:object:method:debug_name_override|debug_name_override]]** | %%Build a read-only name for this object used for debugging.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_del|event_callback_forwarder_del]]** | %%Remove an event callback forwarder for a specified event and object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_priority_add|event_callback_forwarder_priority_add]]** | %%Add an event callback forwarder that will make this object emit an event whenever another object (%%''source''%%) emits it. The event is said to be forwarded from %%''source''%% to this object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_stop|event_callback_stop]]** | %%Stop the current callback call.%% |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_freeze|event_global_freeze]]** | %%Globally freeze events for ALL EFL OBJECTS.%% |
|  ''static'' | **[[:develop:api:efl:object:property:event_global_freeze_count|event_global_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_thaw|event_global_thaw]]** | %%Globally thaw events for ALL EFL OBJECTS.%% |
|  | **[[:develop:api:efl:object:property:finalized|finalized]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:method:invalidate|invalidate]]** | %%Implement this method to perform special actions when your object loses its parent, if you need to.%% |
|  | **[[:develop:api:efl:object:property:invalidated|invalidated]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidating|invalidating]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:name|name]]** //**(get, set)**// | %%The name of the object.%% |
|  | **[[:develop:api:efl:object:method:name_find|name_find]]** | %%Find a child object with the given name and return it.%% |
|  | **[[:develop:api:efl:object:method:provider_find|provider_find]]** | %%Searches upwards in the object tree for a provider which knows the given class/interface.%% |
|  | **[[:develop:api:efl:object:method:provider_register|provider_register]]** | %%Will register a manager of a specific class to be answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
|  | **[[:develop:api:efl:object:method:provider_unregister|provider_unregister]]** | %%Will unregister a manager of a specific class that was previously registered and answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |

===== Events =====

**[[:develop:api:efl:loop_timer:event:timer_tick|timer,tick]]**\\
> %%Event triggered when the specified time as passed.%%
<code c>
EFL_LOOP_TIMER_EVENT_TIMER_TICK(void)
</code>
\\ ==== Inherited ====

^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:event:del|del]]** | %%Object is being deleted. See %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%.%% |
|  | **[[:develop:api:efl:object:event:destruct|destruct]]** | %%Object has been fully destroyed. It can not be used beyond this point. This event should only serve to clean up any reference you keep to the object.%% |
|  | **[[:develop:api:efl:object:event:invalidate|invalidate]]** | %%Object is being invalidated and losing its parent. See %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%%.%% |
|  | **[[:develop:api:efl:object:event:noref|noref]]** | %%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%% |
|  | **[[:develop:api:efl:object:event:ownership_shared|ownership,shared]]** | %%Object has acquired a second reference. It has multiple owners now. Triggered whenever increasing the refcount from one to two, it will not trigger by further increasing the refcount beyond two.%% |
|  | **[[:develop:api:efl:object:event:ownership_unique|ownership,unique]]** | %%Object has lost a reference and only one is left. It has just one owner now. Triggered whenever the refcount goes from two to one.%% |
