~~Title: Efl.App.build_efl_version~~
====== Efl.App.build_efl_version ======

===== Values =====

  * **version** - %%Efl build version%%


\\ {{page>:develop:api-include:efl:app:property:build_efl_version:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property build_efl_version {
    get {}
    values {
        version: const(Efl.Version);
    }
}
</code>

===== C signature =====

<code c>
const Efl_Version efl_app_build_efl_version_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:app:property:build_efl_version|Efl.App.build_efl_version]]

