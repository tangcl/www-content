~~Title: Efl.Input.Clickable: clicked~~

===== Description =====

%%Called when object is in sequence pressed and unpressed by the primary button%%

//Since 1.23//

{{page>:develop:api-include:efl:input:clickable:event:clicked:description&nouser&nolink&nodate}}

===== Signature =====

<code>
clicked: Efl.Input.Clickable_Clicked;
</code>

===== C information =====

<code c>
EFL_INPUT_EVENT_CLICKED(Efl_Input_Clickable_Clicked)
</code>

===== C usage =====

<code c>
static void
on_efl_input_event_clicked(void *data, const Efl_Event *event)
{
    Efl_Input_Clickable_Clicked info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_INPUT_EVENT_CLICKED, on_efl_input_event_clicked, d);
}

</code>
