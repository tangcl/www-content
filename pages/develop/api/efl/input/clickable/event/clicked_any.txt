~~Title: Efl.Input.Clickable: clicked,any~~

===== Description =====

%%Called when object is in sequence pressed and unpressed by any button. The button that triggered the event can be found in the event information.%%

//Since 1.23//

{{page>:develop:api-include:efl:input:clickable:event:clicked_any:description&nouser&nolink&nodate}}

===== Signature =====

<code>
clicked,any: Efl.Input.Clickable_Clicked;
</code>

===== C information =====

<code c>
EFL_INPUT_EVENT_CLICKED_ANY(Efl_Input_Clickable_Clicked)
</code>

===== C usage =====

<code c>
static void
on_efl_input_event_clicked_any(void *data, const Efl_Event *event)
{
    Efl_Input_Clickable_Clicked info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_INPUT_EVENT_CLICKED_ANY, on_efl_input_event_clicked_any, d);
}

</code>
