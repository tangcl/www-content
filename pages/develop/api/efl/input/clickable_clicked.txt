~~Title: Efl.Input.Clickable_Clicked~~

===== Description =====

%%A struct that expresses a click in elementary.%%

//Since 1.23//

{{page>:develop:api-include:efl:input:clickable_clicked:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:input:clickable_clicked:fields&nouser&nolink&nodate}}

  * **repeated** - %%The amount of how often the clicked event was repeated in a certain amount of time%%
  * **button** - %%The Button that is pressed%%

===== Signature =====

<code>
struct Efl.Input.Clickable_Clicked {
    repeated: uint;
    button: uint;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Input_Clickable_Clicked {
    unsigned int repeated;
    unsigned int button;
} Efl_Input_Clickable_Clicked;
</code>
