~~Title: Efl.Input.Key.pressed~~
====== Efl.Input.Key.pressed ======

===== Description =====

%%%%''true''%% if the key is down, %%''false''%% if it is released.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:key:property:pressed:description&nouser&nolink&nodate}}

===== Values =====

  * **val** - %%%%''true''%% if the key is pressed, %%''false''%% otherwise.%%

===== Signature =====

<code>
@property pressed {
    get {}
    set {}
    values {
        val: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_input_key_pressed_get(const Eo *obj);
void efl_input_key_pressed_set(Eo *obj, Eina_Bool val);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:key:property:pressed|Efl.Input.Key.pressed]]

