~~Title: Efl.Task.parent~~
====== Efl.Task.parent ======

===== Description =====

%%The parent of an object.%%

%%Parents keep references to their children and will release these references when destroyed. In this way, objects can be assigned to a parent upon creation, tying their life cycle so the programmer does not need to worry about destroying the child object. In order to destroy an object before its parent, set the parent to %%''NULL''%% and use efl_unref(), or use efl_del() directly.%%

%%The Eo parent is conceptually user set. That means that a parent should not be changed behind the scenes in an unexpected way.%%

%%For example: If you have a widget which can swallow objects into an internal box, the parent of the swallowed objects should be the widget, not the internal box. The user is not even aware of the existence of the internal box.%%

//Since 1.22//
{{page>:develop:api-include:efl:task:property:parent:description&nouser&nolink&nodate}}

===== Values =====

  * **parent** - %%The new parent.%%

//Overridden from [[:develop:api:efl:loop_consumer:property:parent|Efl.Object.parent]] **(set)**.//===== Signature =====

<code>
@property parent {
    get {}
    set {}
    values {
        parent: Efl.Object;
    }
}
</code>

===== C signature =====

<code c>
Efl_Object *efl_parent_get(const Eo *obj);
void efl_parent_set(Eo *obj, Efl_Object *parent);
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:property:parent|Efl.Object.parent]]
  * [[:develop:api:efl:loop_handler:property:parent|Efl.Loop_Handler.parent]]
  * [[:develop:api:efl:canvas:vg:node:property:parent|Efl.Canvas.Vg.Node.parent]]
  * [[:develop:api:efl:canvas:vg:container:property:parent|Efl.Canvas.Vg.Container.parent]]
  * [[:develop:api:efl:input:device:property:parent|Efl.Input.Device.parent]]
  * [[:develop:api:efl:loop_consumer:property:parent|Efl.Loop_Consumer.parent]]
  * [[:develop:api:efl:loop_fd:property:parent|Efl.Loop_Fd.parent]]
  * [[:develop:api:efl:loop_timer:property:parent|Efl.Loop_Timer.parent]]
  * [[:develop:api:efl:task:property:parent|Efl.Task.parent]]
  * [[:develop:api:efl:thread:property:parent|Efl.Thread.parent]]

