~~Title: Efl.Composite_Model.children_count~~
====== Efl.Composite_Model.children_count ======

===== Values =====

  * **count** - %%Current known children count%%


\\ {{page>:develop:api-include:efl:composite_model:property:children_count:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:model:property:children_count|Efl.Model.children_count]] **(get)**.//===== Signature =====

<code>
@property children_count @pure_virtual {
    get {}
    values {
        count: uint;
    }
}
</code>

===== C signature =====

<code c>
unsigned int efl_model_children_count_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:model:property:children_count|Efl.Model.children_count]]
  * [[:develop:api:efl:mono_model_internal:property:children_count|Efl.Mono_Model_Internal.children_count]]
  * [[:develop:api:eldbus:model:property:children_count|Eldbus.Model.children_count]]
  * [[:develop:api:eldbus:model:connection:property:children_count|Eldbus.Model.Connection.children_count]]
  * [[:develop:api:eldbus:model:proxy:property:children_count|Eldbus.Model.Proxy.children_count]]
  * [[:develop:api:eldbus:model:object:property:children_count|Eldbus.Model.Object.children_count]]
  * [[:develop:api:efl:composite_model:property:children_count|Efl.Composite_Model.children_count]]
  * [[:develop:api:efl:container_model:property:children_count|Efl.Container_Model.children_count]]
  * [[:develop:api:efl:filter_model:property:children_count|Efl.Filter_Model.children_count]]
  * [[:develop:api:efl:io:model:property:children_count|Efl.Io.Model.children_count]]
  * [[:develop:api:efl:generic_model:property:children_count|Efl.Generic_Model.children_count]]

