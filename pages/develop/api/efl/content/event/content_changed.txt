~~Title: Efl.Content: content,changed~~

===== Description =====

%%Sent after the content is set or unset using the current content object.%%

//Since 1.22//

{{page>:develop:api-include:efl:content:event:content_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
content,changed: Efl.Gfx.Entity;
</code>

===== C information =====

<code c>
EFL_CONTENT_EVENT_CONTENT_CHANGED(Efl_Gfx_Entity *)
</code>

===== C usage =====

<code c>
static void
on_efl_content_event_content_changed(void *data, const Efl_Event *event)
{
    Efl_Gfx_Entity *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CONTENT_EVENT_CONTENT_CHANGED, on_efl_content_event_content_changed, d);
}

</code>
