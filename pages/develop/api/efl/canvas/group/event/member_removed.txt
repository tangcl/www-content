~~Title: Efl.Canvas.Group: member,removed~~

===== Description =====

%%Called when a member is removed from the group.%%

//Since 1.22//

{{page>:develop:api-include:efl:canvas:group:event:member_removed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
member,removed: Efl.Gfx.Entity;
</code>

===== C information =====

<code c>
EFL_CANVAS_GROUP_EVENT_MEMBER_REMOVED(Efl_Gfx_Entity *)
</code>

===== C usage =====

<code c>
static void
on_efl_canvas_group_event_member_removed(void *data, const Efl_Event *event)
{
    Efl_Gfx_Entity *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CANVAS_GROUP_EVENT_MEMBER_REMOVED, on_efl_canvas_group_event_member_removed, d);
}

</code>
