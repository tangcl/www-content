~~Title: Efl.Canvas.Group: member,added~~

===== Description =====

%%Called when a member is added to the group.%%

//Since 1.22//

{{page>:develop:api-include:efl:canvas:group:event:member_added:description&nouser&nolink&nodate}}

===== Signature =====

<code>
member,added: Efl.Gfx.Entity;
</code>

===== C information =====

<code c>
EFL_CANVAS_GROUP_EVENT_MEMBER_ADDED(Efl_Gfx_Entity *)
</code>

===== C usage =====

<code c>
static void
on_efl_canvas_group_event_member_added(void *data, const Efl_Event *event)
{
    Efl_Gfx_Entity *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CANVAS_GROUP_EVENT_MEMBER_ADDED, on_efl_canvas_group_event_member_added, d);
}

</code>
