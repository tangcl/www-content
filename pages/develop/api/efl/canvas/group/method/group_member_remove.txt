~~Title: Efl.Canvas.Group.group_member_remove~~
====== Efl.Canvas.Group.group_member_remove ======

===== Description =====

%%Removes a member object from a given smart object.%%

%%This removes a member object from a smart object, if it was added to any. The object will still be on the canvas, but no longer associated with whichever smart object it was associated with.%%

%%See also %%[[:develop:api:efl:canvas:group:method:group_member_add|Efl.Canvas.Group.group_member_add]]%%. See also %%[[:develop:api:efl:canvas:group:method:group_member_is|Efl.Canvas.Group.group_member_is]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:canvas:group:method:group_member_remove:description&nouser&nolink&nodate}}

===== Signature =====

<code>
group_member_remove {
    params {
        @in sub_obj: Efl.Canvas.Object;
    }
}
</code>

===== C signature =====

<code c>
void efl_canvas_group_member_remove(Eo *obj, Efl_Canvas_Object *sub_obj);
</code>

===== Parameters =====

  * **sub_obj** //(in)// - %%The member object to remove.%%

===== Implemented by =====

  * [[:develop:api:efl:canvas:group:method:group_member_remove|Efl.Canvas.Group.group_member_remove]]
  * [[:develop:api:efl:canvas:event_grabber:method:group_member_remove|Efl.Canvas.Event_Grabber.group_member_remove]]
  * [[:develop:api:efl:ui:widget:method:group_member_remove|Efl.Ui.Widget.group_member_remove]]

