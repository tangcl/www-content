~~Title: Efl.Canvas.Scene.seats~~
====== Efl.Canvas.Scene.seats ======

===== Description =====

%%Iterate over the available input device seats for the canvas.%%

%%A "seat" is the term used for a group of input devices, typically including a pointer and a keyboard. A seat object is the parent of the individual input devices.%%

//Since 1.22//
{{page>:develop:api-include:efl:canvas:scene:method:seats:description&nouser&nolink&nodate}}

===== Signature =====

<code>
seats @beta @pure_virtual {
    return: iterator<Efl.Input.Device>;
}
</code>

===== C signature =====

<code c>
Eina_Iterator *efl_canvas_scene_seats(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:scene:method:seats|Efl.Canvas.Scene.seats]]

