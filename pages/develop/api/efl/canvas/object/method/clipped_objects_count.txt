~~Title: Efl.Canvas.Object.clipped_objects_count~~
====== Efl.Canvas.Object.clipped_objects_count ======

===== Description =====

%%Returns the number of objects clipped by %%''obj''%%%%

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:method:clipped_objects_count:description&nouser&nolink&nodate}}

===== Signature =====

<code>
clipped_objects_count @const {
    return: uint @no_unused;
}
</code>

===== C signature =====

<code c>
unsigned int efl_canvas_object_clipped_objects_count(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:object:method:clipped_objects_count|Efl.Canvas.Object.clipped_objects_count]]

