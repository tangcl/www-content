~~Title: Efl.Canvas.Object.paragraph_direction~~
====== Efl.Canvas.Object.paragraph_direction ======

===== Description =====

%%This handles text paragraph direction of the given object. Even if the given object is not textblock or text, its smart child objects can inherit the paragraph direction from the given object. The default paragraph direction is %%''inherit''%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:property:paragraph_direction:description&nouser&nolink&nodate}}

===== Values =====

  * **dir** - %%Paragraph direction for the given object.%%

===== Signature =====

<code>
@property paragraph_direction {
    get {}
    set {}
    values {
        dir: Efl.Text_Bidirectional_Type;
    }
}
</code>

===== C signature =====

<code c>
Efl_Text_Bidirectional_Type efl_canvas_object_paragraph_direction_get(const Eo *obj);
void efl_canvas_object_paragraph_direction_set(Eo *obj, Efl_Text_Bidirectional_Type dir);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:object:property:paragraph_direction|Efl.Canvas.Object.paragraph_direction]]
  * [[:develop:api:efl:canvas:text:property:paragraph_direction|Efl.Canvas.Text.paragraph_direction]]
  * [[:develop:api:efl:canvas:group:property:paragraph_direction|Efl.Canvas.Group.paragraph_direction]]
  * [[:develop:api:efl:canvas:layout:property:paragraph_direction|Efl.Canvas.Layout.paragraph_direction]]

