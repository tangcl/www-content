~~Title: Efl.Canvas.Object.hint_size_combined_max~~
====== Efl.Canvas.Object.hint_size_combined_max ======

===== Values =====

  * **sz** - %%Maximum size (hint) in pixels.%%


\\ {{page>:develop:api-include:efl:canvas:object:property:hint_size_combined_max:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:gfx:hint:property:hint_size_combined_max|Efl.Gfx.Hint.hint_size_combined_max]] **(get)**.//===== Signature =====

<code>
@property hint_size_combined_max @pure_virtual {
    get {}
    values {
        sz: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_gfx_hint_size_combined_max_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:hint:property:hint_size_combined_max|Efl.Gfx.Hint.hint_size_combined_max]]
  * [[:develop:api:efl:canvas:object:property:hint_size_combined_max|Efl.Canvas.Object.hint_size_combined_max]]

