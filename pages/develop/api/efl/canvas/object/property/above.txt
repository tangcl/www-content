~~Title: Efl.Canvas.Object.above~~
====== Efl.Canvas.Object.above ======



\\ {{page>:develop:api-include:efl:canvas:object:property:above:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:gfx:stack:property:above|Efl.Gfx.Stack.above]] **(get)**.//===== Signature =====

<code>
@property above @pure_virtual {
    get {
        return: Efl.Gfx.Stack @no_unused;
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Stack *efl_gfx_stack_above_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:stack:property:above|Efl.Gfx.Stack.above]]
  * [[:develop:api:efl:canvas:vg:node:property:above|Efl.Canvas.Vg.Node.above]]
  * [[:develop:api:efl:canvas:object:property:above|Efl.Canvas.Object.above]]

