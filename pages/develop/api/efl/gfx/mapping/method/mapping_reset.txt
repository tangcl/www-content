~~Title: Efl.Gfx.Mapping.mapping_reset~~
====== Efl.Gfx.Mapping.mapping_reset ======

===== Description =====

%%Resets the map transformation to its default state.%%

%%This will reset all transformations to identity, meaning the points' colors, positions and UV coordinates will be reset to their default values. %%[[:develop:api:efl:gfx:mapping:method:mapping_has|Efl.Gfx.Mapping.mapping_has]]%% will then return %%''false''%%. This function will not modify the values of %%[[:develop:api:efl:gfx:mapping:property:mapping_smooth|Efl.Gfx.Mapping.mapping_smooth]]%% or %%[[:develop:api:efl:gfx:mapping:property:mapping_alpha|Efl.Gfx.Mapping.mapping_alpha]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:gfx:mapping:method:mapping_reset:description&nouser&nolink&nodate}}

===== Signature =====

<code>
mapping_reset {}
</code>

===== C signature =====

<code c>
void efl_gfx_mapping_reset(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:mapping:method:mapping_reset|Efl.Gfx.Mapping.mapping_reset]]

