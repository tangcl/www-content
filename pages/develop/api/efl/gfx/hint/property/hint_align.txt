~~Title: Efl.Gfx.Hint.hint_align~~
====== Efl.Gfx.Hint.hint_align ======

===== Description =====

%%Hints for an object's alignment.%%

%%These are hints on how to align an object inside the boundaries of a container/manager. Accepted values are in the 0.0 to 1.0 range.%%

%%For the horizontal component, 0.0 means the start of the axis in the direction that the current language reads, 1.0 means the end of the axis.%%

%%For the vertical component, 0.0 to the top, 1.0 means to the bottom.%%

%%This is not a size enforcement in any way, it's just a hint that should be used whenever appropriate.%%

<note>
%%Default alignment hint values are 0.5, for both axes.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:gfx:hint:property:hint_align:description&nouser&nolink&nodate}}

===== Values =====

  * **x** - %%Double, ranging from 0.0 to 1.0, where 0.0 is at the start of the horizontal axis and 1.0 is at the end.%%
  * **y** - %%Double, ranging from 0.0 to 1.0, where 0.0 is at the start of the vertical axis and 1.0 is at the end.%%

===== Signature =====

<code>
@property hint_align @pure_virtual {
    get {}
    set {}
    values {
        x: double;
        y: double;
    }
}
</code>

===== C signature =====

<code c>
void efl_gfx_hint_align_get(const Eo *obj, double *x, double *y);
void efl_gfx_hint_align_set(Eo *obj, double x, double y);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]
  * [[:develop:api:efl:canvas:object:property:hint_align|Efl.Canvas.Object.hint_align]]

