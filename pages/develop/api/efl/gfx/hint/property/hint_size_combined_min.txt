~~Title: Efl.Gfx.Hint.hint_size_combined_min~~
====== Efl.Gfx.Hint.hint_size_combined_min ======

===== Values =====

  * **sz** - %%Minimum size (hint) in pixels.%%


\\ {{page>:develop:api-include:efl:gfx:hint:property:hint_size_combined_min:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property hint_size_combined_min @pure_virtual {
    get {}
    values {
        sz: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_gfx_hint_size_combined_min_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:hint:property:hint_size_combined_min|Efl.Gfx.Hint.hint_size_combined_min]]
  * [[:develop:api:efl:canvas:object:property:hint_size_combined_min|Efl.Canvas.Object.hint_size_combined_min]]

