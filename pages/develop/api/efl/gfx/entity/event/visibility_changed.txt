~~Title: Efl.Gfx.Entity: visibility,changed~~

===== Description =====

%%Object's visibility state changed, the event value is the new state.%%

//Since 1.22//

{{page>:develop:api-include:efl:gfx:entity:event:visibility_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
visibility,changed: bool;
</code>

===== C information =====

<code c>
EFL_GFX_ENTITY_EVENT_VISIBILITY_CHANGED(Eina_Bool)
</code>

===== C usage =====

<code c>
static void
on_efl_gfx_entity_event_visibility_changed(void *data, const Efl_Event *event)
{
    Eina_Bool info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_GFX_ENTITY_EVENT_VISIBILITY_CHANGED, on_efl_gfx_entity_event_visibility_changed, d);
}

</code>
